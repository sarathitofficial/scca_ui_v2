import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginLogoutAuditComponent } from './login-logout-audit.component';

describe('LoginLogoutAuditComponent', () => {
  let component: LoginLogoutAuditComponent;
  let fixture: ComponentFixture<LoginLogoutAuditComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [LoginLogoutAuditComponent]
    });
    fixture = TestBed.createComponent(LoginLogoutAuditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
