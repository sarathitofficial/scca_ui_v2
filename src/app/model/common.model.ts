export class DataSource {
    action: string;
    id: number;
    name: string;
    type: string;
    primaryHost: string;
    primaryPort: string;
    primaryDatabaseName: string;
    primaryUsername: string;
    primaryPassword: string;
    secondaryHost: string;
    secondaryPort: string;
    secondaryDatabaseName: string;
    secondaryUsername: string;
    secondaryPassword: string;
    isEnable: boolean;
    timezone: string;
    description: string;
    failsafe: boolean;
    status: string;
    userid: number;
  
    constructor() {
      this.action = "addDataSource";
      this.id = 0;
      this.name = "";
      this.type = "";
      this.primaryHost = "";
      this.primaryPort = "";
      this.primaryDatabaseName = "";
      this.primaryUsername = "";
      this.primaryPassword = "";
      this.secondaryHost = "";
      this.secondaryPort = "";
      this.secondaryDatabaseName = "";
      this.secondaryUsername = "";
      this.secondaryPassword = "";
      this.isEnable = true;
      this.timezone = "";
      this.description = "";
      this.failsafe = false;
      this.status = "active";
      this.userid = 0;
    }
  }
  

  export class Directory {
    action: string = "addDirectory";
    id: number = 0;
    name: string = "";
    type: string = "reportbuilder";
    description: string = "";
    parentid: number = 0;
    rootdir: string = "";
    status: string = "active";
    userid: number = 1;
  
    constructor() {}
  }
  
  export class Parameter {
    id: number = 0;
    reportbuilderid: { name: string; id: null }|number  = { name: '', id: null };
    name: string = "";
    alias: string = "";
    datatype: string = "";
    valuetype: string = "";
    value: string = "";
    collectionid: { name: string; id: null }|number  = { name: '', id: null };
    allownull: boolean = false;
    stringInput?: string = "";
    decimalInput?: number = 0;
    datetimeInput?: string = '';
    collectionTarget?: any[] = [];
    collectionSource?: any[] = [];
  
    constructor() {}
  }
  
  export class Field {
    id: number = 0;
    reportbuilderid: number = 0;
    name: string = "";
    alias: string = "";
    datatype: { label: string; value: string } = { label: '', value: '' };
    iscustomfield: boolean = false;
    customfieldformula: string = "";
    iscustomresult: boolean = false;
    result: { label: string; value: string } = { label: '', value: '' };
    resultFormula: string = "";
  
    constructor() {}
  }
  
  export class ReportBuilder {
    action: string = "addReportBuilder";
    id: number = 0;
    name: string = "";
    reporttype: { label: string; value: string } = { label: '', value: '' };
    duration: number=0;
    description: string = "";
    datasourceid:{ name: string; id: number} | number  = { name: '', id: 0 };
    querytype: { label: string; value: string } = { label: '', value: '' };
    query: string = "";
    parameters: Parameter[] = [];
    fields: Field[] = [];
    userid: number = 1;
    createdby: string = "";
    createddate: string = "";
    modifiedby: string = "";
    modifieddate: string = "";
  
    constructor() {}
  }
  
  export class Report {
    action: string = "addReport";
    id: number = 0;
    name: string = "";
    reportbuilderfieldids: any[] = [];
    description: string = "";
    reportbuilderid: { name: string; id: number }|number = { name: '', id: 0 };
    userid: number = 1;
    createdby: string = "";
    createddate: string = "";
    modifiedby: string = "";
    modifieddate: string = "";
  
    constructor() {}
  }

  export class CollectionMaster{
    collectionList :any[]=[];
    directoryList :any[]=[];
    constructor(){
    }
  }

  export class Collection{
    action: string = "addCollection";
    id: number = 0;
    name: string = "";
    description: string = "";
    datasourceid:{ name: string; id: number} | number  = { name: '', id: 0 };
    query:string="";
    collectionvalues: CollectionValue[]=[];
    parentid:number=0;
    parentpath:string="";
    userid: number = 1;
    status :string ="";
    createdby: string = "";
    createddate: string = "";
    modifiedby: string = "";
    modifieddate: string = "";
  }

  export class CollectionValue{
    id: number = 0;
    name: string = "";
  }
  