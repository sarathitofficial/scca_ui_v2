import { Component, ElementRef, OnInit, Renderer2, ViewChild, OnDestroy, NgZone, TemplateRef, ViewContainerRef } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ToastrService } from 'ngx-toastr';
import { MessageService } from 'primeng/api';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
import { HttpClient } from '@angular/common/http';
// Shared service import
import { SharedService, ReportTab } from 'src/app/services/shared.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { environment } from 'src/assets/environments/environment';
import { AppComponent } from '../app.component';
import {Directory,Report,Field,Parameter} from '../model/common.model';
import { Subscriber } from 'rxjs';




@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss'],
})
export class ReportComponent {
  @ViewChild('staticTabs', { static: false }) staticTabs?: TabsetComponent;
  @ViewChild('tabConfirmationTemplate', { read: TemplateRef }) tabConfirmationTemplate!: TemplateRef<any>;

  addEditFolderModalRef!: BsModalRef;
  parameterFilterModalRef!: BsModalRef;
  addEditReportModalRef!: BsModalRef;
  fieldModalRef!: BsModalRef;
  customFieldModalRef!: BsModalRef;
  tabConfirmationModalRef!: BsModalRef;

  tabList: ReportTab[] = [];
  directoryPath: any[] = [];
  reportTableHeader: any[]=[];
  filteredTableHeader:any[]=[];
  reportTableData: any[]=[];
  

  // String type variables
  spinnerComponent='';
  searchKey: string = '';
  currentStepperPage = '';
  selectedParameterIndex: any;
  selectedFieldIndex: any;
  isEdit: any;
  isFieldEdit: any;
  selectedDeletePopup = 'folder';

  showRootFolders = true;
  showErrors = false;
  isDirectoryFieldsValid = false;
  isBasicFieldsValid = false;
  isConfigFieldsValid = false;
  isParameterFieldsValid = false;
  isOutputFieldsValid = false;
  showAddEditFieldsPickList = false;

  // Object Type variables
  directory: any;
  report: any;
  reportbuilder: any;
  parameter: any;
  field: any;
  reportType = [{ label: '', value: '' }];
  queryTypes = [{ label: '', value: '' }];
  reportbuilderFieldResult = [{ label: '', value: '' }];
  reportbuilderFieldDataTypes = [{ label: '', value: '' }];
  selectedDataSource: any = {};


  //Boolean values
  showTabPanel: boolean = false;
  currentStep = 1;
  numSteps = 2;
  configPage: boolean = false;
  basicDetailspage: boolean = false;
  spinnerTriggered: boolean = false;


  // List type variables
  reportbuilderList: any[] = [];
  collectionList: any[] = [];
  dataSourceList: any[] = [];
  parameterList: any[] = [];
  fieldList: any[] = [];
  targetFieldList: any[] = [];
  masterData: any;

  recordsPerPage:any;

  constructor(
    private _toastr: ToastrService,
    private _apiService: ApiService,
    public sharedService: SharedService,
    public messageService: MessageService,
    private renderer: Renderer2,
    private modalService: BsModalService,
    private appComponent: AppComponent
  ) { }


  nameAtoZ = "./assets/images/nameAtoZ.png";
  nameZtoA = "./assets/images/nameZtoA.png";
  DateOtoN = "./assets/images/DateOtoN.png";
  DateNtoO = "./assets/images/DateNtoO.png";
  categories: any[] = [
    {
      name: 'Name (A to Z)',
      key: 'A',
      img: "./assets/images/nameAtoZ.png"
    },
    {
      name: 'Name (Z to A)',
      key: 'Z',
      img: "./assets/images/nameZtoA.png"
    },
    {
      name: 'Date (Oldest to Newest)',
      key: 'O',
      img: "./assets/images/DateOtoN.png"
    },
    {
      name: 'Date (Newest to Oldest)',
      key: 'N',
      img: "./assets/images/DateNtoO.png"
    }
  ];

  ngOnInit() {
    
    this.appComponent.isLoggedIn = true;
    this.initializeStaticValues();
    this.pathSubscriber();
    if (!this.showRootFolders) {
      this.clear();
      this.loadDataList()
    }
  };

  ngOnDestroy(){
  this.clearRealtimeTimer(this.tabList.findIndex(x=>x.active));
  }

  clearRealtimeTimer(index:any){
    if(index!=null && index!=undefined && index>-1){
    clearInterval(this.tabList[index]?.durationTimer);
    clearInterval(this.tabList[index]?.secondsTimer);
    this.tabList[index].timer='';
    this.tabList[index].durationTimer=null;
    this.tabList[index].secondsTimer=null;
    }
  }

  //Triggered whenever the directory path is changed
  pathSubscriber() {
    this.sharedService.getDirectoryPath().subscribe(path => {
      this.clear();
      this.masterData={reportList:[],directoryList:[]};
      if (path?.length > 1) {
        this.showRootFolders = false;
        this.reportType[0] = {
          label: this.sharedService.getDirectoryPath().value[1].name,
          value: this.sharedService.getDirectoryPath().value[1].name
        };
        this.loadDataList();
      }
      else {
        this.showRootFolders = true;
        this.masterData.directoryList = environment.reportRootPath;
      }
    });
  }


  //Fields to be initialized once when the component is loaded
  initializeStaticValues() {
    //Dropdown values one time initialization
    this.recordsPerPage=environment.reportPageSizeCollection;
    this.queryTypes = environment.queryTypes;
    this.reportbuilderFieldResult = environment.reportbuilderFieldResult;
    this.reportbuilderFieldDataTypes = environment.reportbuilderFieldDataTypes;
    this.masterData = { reportList: [], directoryList: [] }
    this.masterData.directoryList = environment.reportbuilderRootPath;
  }

  // Method to reset/clear all fields.
  clear() {
    this.reportbuilderList = [];
    this.dataSourceList = [];
    this.collectionList = [];
    this.searchKey = '';
    this.directory = new Directory();
    this.report =new Report();
  };

  loadDataList() {
    this.getScreenDirectories();
    this.getAllReports();
    this.getAllReportBuilderIdName();
  }

  //Check data exist or not
  dataNotFound(){
    if(this.masterData?.reportList?.length>0 || this.masterData?.directoryList?.length>0){
      return false;
    }
    return true
  }

  // Method to call when search icon is pressed.
  search = () => {
    let searchkey=this.searchKey;
    this.clear();
    this.searchKey=searchkey;
    this.loadDataList();
  };

  // On change in sorting petion.
  onSortChange = (field: string, order: string) => {
    this.dataSourceList = this.sharedService.sorting(
      [],
      field,
      order
    );
  };


  //Retrieve all the report builders related to parent id and path from database
  getAllReports() {
    this.masterData.reportList = [];
    this._apiService
      .makeAPICall({
        action: 'getAllReports',
        searchkey: this.searchKey,
        parentid: this.sharedService.getDirectoryPath().value.slice(-1)[0].id,
        parentpath: (this.sharedService.getDirectoryPath().value.map(x => x.name).join('/')) + '/',
        loader: 'MainPageLoader'
      })
      .subscribe((response) => {
        if (response && response.status) {
          if (response?.data?.length > 0) {
            this.masterData.reportList = response.data;
          } else {
          }
          console.log('getAllReports response', response);
        }
      });
  }

  //Retrieve all the directories related to parent id and path from database
  getScreenDirectories() {
    this.masterData.directoryList=[];
    this._apiService
      .makeAPICall({
        action: 'getScreenDirectories',
        searchkey: this.searchKey,
        parentid: this.sharedService.getDirectoryPath().value.slice(-1)[0].id,
        rootdir: (this.sharedService.getDirectoryPath().value.map(x => x.name).join('/')) + '/',
        loader: 'MainPageLoader'

      })
      .subscribe((response) => {
        if (response && response.status) {
          if (response?.data?.length > 0) {
            this.masterData.directoryList = response.data;
          } else {
          }
          console.log('getscreendirectories response', response);
        }
      });
  }

  //Go back in main folder
  mainPageBackStep(){
  const selectedPath=this.sharedService.getDirectoryPath().value;
  selectedPath.pop();
  this.sharedService.setDirectoryPath(selectedPath);
  }


  //Trigger when the next button is clicked in popup
  //Handle all the steppers with validation
  async nextStep() {

    console.log('report data', this.report);

    this.showErrors = true;

    switch (this.currentStepperPage) {
      case 'basic':
        await this.ReportFieldsValidation('basic');
        if (!this.isBasicFieldsValid) {
          return;
        }
        else {
          this.spinnerComponent='reportbasicfieldnext';
          if (!this.showAddEditFieldsPickList)
           await this.getFieldsByReportBuilder();
          this.showErrors = false;
          this.spinnerComponent='';
        }
        break;

      default:
        // Optional: Handle unexpected values of currentStepperPage
        console.log(`Unknown stepper page: ${this.currentStepperPage}`);
        break;
    }



    this.currentStep++;
    console.log("current step is:", this.currentStep);
    if (this.currentStep > this.numSteps) {
      this.currentStep = 1;
    }


    const steps = document.querySelectorAll('.step');

    steps.forEach((step: any, index: any) => {
      const stepNum = index + 1;
      if (stepNum === this.currentStep) {
        this.renderer.addClass(step, 'editing');
      } else {
        this.renderer.removeClass(step, 'editing');
      }
      if (stepNum < this.currentStep) {
        this.renderer.addClass(step, 'done');
      } else {
        this.renderer.removeClass(step, 'done');
      }
    });

    if (this.currentStep === 2) {
      this.currentStepperPage = 'fields';
    } else {
      this.currentStepperPage = 'basic';
    }
  }


  //Trigger when the back button is clicked in popup
  //validations also being done here
  backStep(step: any) {
    if (this.currentStep > 1) {
      this.currentStep--;
    } else {
      this.currentStep = this.numSteps;
    }
    const steps = document.querySelectorAll('.step');

    steps.forEach((step: any, index: any) => {
      const stepNum = index + 1;
      if (stepNum === this.currentStep) {
        this.renderer.addClass(step, 'editing');
      } else {
        this.renderer.removeClass(step, 'editing');
      }
      if (stepNum < this.currentStep) {
        this.renderer.addClass(step, 'done');
      } else {
        this.renderer.removeClass(step, 'done');
      }
    });
    if (this.currentStep === 2) {
      this.currentStepperPage = 'config';
    } else if (this.currentStep === 3) {
      this.currentStepperPage = 'params';
    } else if (this.currentStep === 4) {
      this.currentStepperPage = 'fields';
    } else {
      this.currentStepperPage = 'basic';
    }

  }




  //Directory or Folder Methods

  addEditFolderPopup(action: string, template: TemplateRef<any>) {

    if (action === 'open') {
      this.resetAddEditFolderPopup();
      this.addEditFolderModalRef = this.modalService.show(template, {
        backdrop: 'static',
        keyboard: false,
      });



      if (this.isEdit) {
        console.log('Edit directory', this.directory);

      }
      else {

        this.directory =new Directory();

        console.log('Add directory', this.directory);
      }
    } else {
      this.addEditFolderModalRef?.hide()
    }
  }

  //Reset folder fields
  resetAddEditFolderPopup() {
    this.spinnerComponent='';
    this.showErrors = false;
    this.isDirectoryFieldsValid = false;
  }

  directoryFieldValidation() {
    if (this.directory.name != '') {
      this.isDirectoryFieldsValid = true;
      return;
    }
  }

  //Method to form the object or model for Editing Folder/Directory data
  //Action - edit
  mapEditDirectory(data: any) {
    this.directory =new Directory();
    this.directory = {
      action: 'editDirectory',
      id: data.id,
      name: data.name,
      type: data.type,
      description: data.description,
      parentid: data.parentid,
      rootdir: data.rootdir,
      status: data.status,
      userid: 1
    }
  }

  // Add new or edit existing directory
  async storeDirectory() {

    this.spinnerComponent='storeDirectory';

    this.showErrors = true;
    this.directoryFieldValidation();

    if (!this.isDirectoryFieldsValid) {
      this._toastr.info('Please Fill the required fields');
      return;
    }

    if (this.isEdit) {

    }
    else {
      this.directory.parentid = this.sharedService.getDirectoryPath().value.slice(-1)[0].id;
      this.directory.rootdir = (this.sharedService.getDirectoryPath().value.map(x => x.name).join('/')) + '/';
    }

    console.log('store data', this.directory);

    try{
    //ADD - if directory fields are validated , Store it
    //EDIT -if directory fields are validated, save it
    let response=await this._apiService
      .makeAPICallAsync(this.directory);

        if (response.status && response.data.message.split('-')[1] === 'true') {
          this._toastr.success(('Folder ' + (this.isEdit ? 'Edited' : 'Added' + ' successfully')));
          this.clear();
          this.getScreenDirectories();
          this.addEditFolderModalRef?.hide();
          
        }
        else {
          this._toastr.error((this.isEdit ? 'Update Error' : 'Insert Error'), response.data.message.split('-')[0], { disableTimeOut: true });
        }
        console.log('storeDirectory response', response);
    }
    catch(error){
      console.log('storeDirectory error',error);
    }
    finally{
      this.spinnerComponent='';
    }
  }

  // Show or hide the delete popup commonly for both folder and files  
  toggleDeleteModal(action: string, template: TemplateRef<any>) {
    if (action === 'open')
      this.modalService.show(template, {
        backdrop: 'static',
        keyboard: false,
      });
    else if (action === 'close')
      this.modalService.hide();
  }

  
  // Method to Delete directory from Database.
  async deleteDirectory() {
    this.spinnerComponent = 'deleteDirectory';
    this.directory.action = 'deleteDirectory';

    try {

      let response = await this._apiService
        .makeAPICallAsync(this.directory);
      if (response.status && response.data.message.split('-')[1] === 'true') {
        this._toastr.success('Folder deleted successfully');
        this.clear();
        this.getScreenDirectories();
        this.modalService.hide();
      }
      else {
        this._toastr.error('Delete error', response.data.message.split('-')[0], { disableTimeOut: true });
      }
      console.log('deleteDirectory response', response);
    }
    catch (error) {
     console.log('deleteDirectory error',error)
    }
    finally {
      this.spinnerComponent = '';
    }
  };



  //Method to validation report builder all four stages
  async ReportFieldsValidation(stepper: string) {
    switch (stepper) {

      case 'basic':
        if (this.report.name !== '' && (this.report.reportbuilderid.name !== '')) {
          this.isBasicFieldsValid = true;
        } else {
          this.isBasicFieldsValid = false;
        }
        break;

      case 'fields':
        if (this.targetFieldList = []) {
          this.isOutputFieldsValid = true;
        } else {
          this.isOutputFieldsValid = false;
        }
        break;

      default:
        // Handle unexpected stepper values if necessary
        console.log('Unexpected stepper value:', stepper);
        break;
    }
  }


  //Reset the necessary values for adding or editing reports.
  resetAddEditReportPopup() {
    this.showAddEditFieldsPickList = false;
    this.isBasicFieldsValid = false;
    this.isOutputFieldsValid = false;
    this.showErrors = false;
    this.currentStepperPage = 'basic';
    this.currentStep = 2;
    this.spinnerComponent='';
    this.backStep(1);
  }

  //Show or hide Add/edit report popup based on the action param
  //action - open - show popup
  //action - clse - close popup
  addEditReportPopup(action: string, template: TemplateRef<any>) {

    if (action === 'open') {
      this.resetAddEditReportPopup();
      this.addEditReportModalRef = this.modalService.show(template, {
        backdrop: 'static',
        keyboard: false,
      });

      if (this.isEdit) {

        // Move to stepper one and add/remove the stepper class
        this.currentStep = 1;
        const steps = document.querySelectorAll('.step');

        steps.forEach((step: any, index: any) => {
          if (index === 0) {
            this.renderer.addClass(step, 'editing');
            this.renderer.removeClass(step, 'done');
          } else {
            this.renderer.addClass(step, 'done');
            this.renderer.removeClass(step, 'editing');
          }
        });
        console.log('Edit Report', this.report);
      }
      else {
        this.report = new Report();
        console.log('Add Report', this.report);
      }
    }

    //Action- close- Hide the popup
    else {
      this.addEditReportModalRef?.hide()
    }
  }


  //Method to form the reportbuilder data for edit action
  mapReportEditData(report: any) {

    const rb = this.reportbuilderList.find(x => x.id = typeof report.reportbuilderid === 'number' ?
      report.reportbuilderid : report.reportbuilderid.id);
    this.report = new Report();
    this.report = {
      action: 'editReport',
      id: report.id,
      name: report.name,
      description: report.description,
      reportbuilderid: { name: rb.name, id: rb.id },
      parentid: report.parentid,
      parentpath: report.parentpath,
      createdby: report.createdby,
      createddate: report.createddate,
      modifiedby: report.modifiedby,
      modifieddate: report.modifieddate,
    }
  }

  //Method to store the added or edited reportbuilder in database
  async storeReport() {
    this.spinnerComponent='submitAddEditReport';

    if (!this.isEdit) {
      this.report.reportbuilderid = this.report.reportbuilderid.id;
      this.report.reportbuilderfieldids = this.targetFieldList.map((x: { id: any; }) => x.id).join(',');
      this.report.parentpath = (this.sharedService.getDirectoryPath().value.map(x => x.name).join('/')) + '/';
      this.report.parentid = this.sharedService.getDirectoryPath().value.slice(-1)[0].id;
    }

    this.ReportFieldsValidation('basic');
    if (!this.isBasicFieldsValid) {
      this._toastr.info('Please fill the required fields');
      return;
    }

    this.ReportFieldsValidation('fields');
    if (!this.isOutputFieldsValid) {
      this._toastr.info('Atleast select one field');
      return;
    }


    try{
    let response= await this._apiService
      .makeAPICallAsync(this.report);

        if (response.status && response.data.message.split('-')[1] === 'true') {
          this._toastr.success(('Report ' + (this.isEdit ? 'updated' : 'inserted' + ' successfully')));
          this.clear();
          this.getAllReports();
          this.addEditReportModalRef?.hide()
        }
        else {
          this._toastr.error((this.isEdit ? 'Update Error' : 'Insert Error'), response.data.message.split('-')[0], { disableTimeOut: true });
        }
        console.log('storereport response', response);
       this.spinnerComponent='';

    }
    catch(error){
      console.log('storereport error', error);
      this.spinnerComponent='';
    }

    console.log('report', this.report);
  }


  // delete the existing report from database
  async deleteReport() {
    this.spinnerComponent = 'deleteReport';
    console.log('report', this.report);
    this.report.action = 'deleteReport';
    try {
      let response =await this._apiService
        .makeAPICallAsync(this.report);

      if (response.status && response.data.message.split('-')[1] === 'true') {
        this._toastr.success('Report deleted successfully');
        this.clear();
        this.getAllReports();
        this.modalService.hide();
      }
      else {
        this._toastr.error('Delete error', response.data.message.split('-')[0], { disableTimeOut: true });
      }
      console.log('response', response);
    }
    catch (error) {
      console.log('deletereport error', error);
    }
    finally {
      this.spinnerComponent = '';
    }
  }

  // Method to Get All Datasource from Database.
  getAllReportBuilderIdName() {

    this._apiService
      .makeAPICall({
        action: 'getAllReportBuilderIdName',
        reporttype : this.reportType[0].label,
        loader: 'MainPageLoader'
      })
      .subscribe((response: any) => {
        if (response && response.status) {
          if (response.data && response.data?.length > 0) {
            this.reportbuilderList = response.data;
            console.log("getAllReportBuilderIdName", this.reportbuilderList);
            if (this.isEdit && this.report.reportbuilderid != undefined) {
              this.report.reportbuilderid = this.reportbuilderList.find(x => x.id === this.report.reportbuilderid.id)
            }
          } else {
            
          }
        }
        console.log("getAllReportBuilderIdName response", response);
      });
  };

  // Method to Get All Datasource from Database.
  getAllCollectionIdName = () => {
    this._apiService
      .makeAPICall({
        action: 'getAllCollectionIdName',
        searchKey: this.searchKey,
      })
      .subscribe((response: any) => {
        if (response && response.status) {
          if (response.data?.length > 0) {
            this.collectionList = response.data;
            console.log("collection list id name", this.collectionList);
          } else {
          }
        }
      });
  };



  //Method to get report builder 
  async getReportBuilder(id: number, name: string) {
    let response = await this._apiService
      .makeAPICallAsync({
        action: 'getReportBuilder',
        id: id
      });

    if (response && response.status) {
      this.reportbuilder = response.data;
      console.log('selected report builder', this.reportbuilder);
      this.report.reportbuilderid = { name: name, id: id };

      if (this.tabList?.length > 0) {
        this.tabList[this.tabList.findIndex(x => x.active)].reportbuilder = response.data[0];
      }
    }
    else {
      console.log('selected report builder response', response);
    }
  }

  //Method to get Fields by report builder
  async getFieldsByReportBuilder() {

   let response= await this._apiService
      .makeAPICallAsync({
        action: 'getFieldsByReportBuilder',
        reportbuilderid: this.report?.reportbuilderid?.id
      });

      
      console.log('getFieldsByReportBuilder reponse', response);

        if (response && response.status) {


          // Insert fields to field list
          for (let i = 0; i < response.data?.length; i++) {
            let field =new Field();
            field.id = response.data[i].id;
            field.name = response.data[i].name;
            field.alias = response.data[i].alias;
            field.datatype = {
              label: response.data[i].datatype,
              value: response.data[i].datatype
            };

            if (this.isEdit && this.report.reportbuilderfieldids.split(',').include(response.data[i].id)) {
              this.targetFieldList.push(field)
            }
            else {
              this.fieldList.push(field);
            }
          }

          this.showAddEditFieldsPickList = true;

          console.log('getFieldsByReportBuilder', this.fieldList);


        } else {

        }


  }

  //Method to get Parameter by report builder
  async getParametersByReportBuilder() {
   let response= await this._apiService
      .makeAPICallAsync({
        action: 'getParametersByReportBuilder',
        reportbuilderid: this.report.reportbuilderid.id
      });
        if (response && response.status) {

          // Insert parameters to parameter list
          for (let i = 0; i < response.data?.length; i++) {
            let param = new Parameter();
            param.id = response.data[i].id;
            param.reportbuilderid = response.data[i].reportbuilderid;
            param.name = response.data[i].name;
            param.alias = response.data[i].name;
            param.datatype = response.data[i].datatype;
            param.value = response.data[i].value;
            param.allownull = response.data[i].allownull;
            param.collectionid = response.data[i] = { name: '', id: response.data[i].collectionid };

            this.parameterList.push(param);
          }

          if (this.tabList?.length > 0) {
            this.tabList[this.tabList.findIndex(x => x.active)].parameters = this.parameterList;
          }

          await this.getCollectionQueryValues();

          console.log('getParametersByReportBuilder', this.parameterList);

        } else {
        }
      console.log('getParametersByReportBuilder reponse', response);
  }

  async getCollectionQueryValues() {
   let response=await this._apiService.makeAPICallAsync({
      action: 'getCollectionQueryValues',
      id: this.parameterList.filter(x => x.collectionid.id != null && x.collectionid.id > 0).map(x => x.collectionid.id).join(',')});
    
      if (response && response.status) {
        console.log('getCollectionQueryValues response', response.data);
        for (let i = 0; i < response.data?.length; i++) {
          let index = this.parameterList.findIndex(x => x.collectionid.id != null && x.collectionid.id == response.data[i].id);
          this.parameterList[index].collectionSource = response.data[i].values;
        }

        console.log('getCollectionQueryValues', this.parameterList);
      } else {
      }
      console.log('getCollectionQueryValues response', response);
  }

  resetParameterFilterFields() {
    this.parameterList = [];
  }

  //Show or hide the parameter popup
  async parameterFilterPopup(action: string, template: TemplateRef<any>) {
    if (action === 'open') {    
    this.spinnerComponent='reportOverviewRun';
      this.resetParameterFilterFields();
      await this.getParametersByReportBuilder();
      this.spinnerComponent='';
      this.parameterFilterModalRef = this.modalService.show(template, {
        backdrop: 'static',
        keyboard: false
      });
    } else if(action==='close') {
      this.parameterFilterModalRef.hide();
    }
    else if(action==='reportopen'){
      this.parameterFilterModalRef = this.modalService.show(template, {
        backdrop: 'static',
        keyboard: false
      });
    }
  }


  //Render report table and its controls
  async reportTablePage() {
    this.spinnerComponent='reportParameterRun';
    let index=this.tabList.findIndex(x => x.active);
    const rbres = await this.getReportBuilder(this.report.reportbuilderid.id, this.report.reportbuilderid.name);
    const tableres = await this.getReportTableData(index);
    this.tabList[index].showReportTable = true;
    this.parameterFilterModalRef?.hide();
    this.spinnerComponent='';
  }



  // Retrive the report data table dynamically based on the parameters and report builder
  async getReportTableData(index:any) {
    const tabIndex=this.tabList.findIndex(x => x.active);
    //Bind direct id from dropdown objects
    let tab = Object.assign({}, this.tabList[this.tabList.findIndex(x => x.active)]);
    let response = await this._apiService.makeAPICallAsync({
      action: 'getReportDataTable',
      isRefresh: false,
      report: tab.data,
      reportbuilder: tab.reportbuilder,
      parameters: tab.parameters
    });

    if (response && response.status) {
      console.log('getReportDataTable response', response.data);
      this.tabList[index].tableHeader = response.data.header;
      this.tabList[index].tableSelectedHeader  = response.data.header;
      this.tabList[index].tableRows = response.data.data;
      this.tabList[index].reportStructure=response.data.reportstructure;
      console.log('getReportDataTable', this.parameterList);

      if(this.tabList[index].reportbuilder.reporttype==='RealTime'){
        this.secondsTimer(index,this.tabList[index].reportbuilder.duration);
        this.realTimeCountDownTimer(index);
      }

    } else {
     
    }
    console.log('getReportDataTable response', response);
  }

  //Method for refreshing table or automatic realtime
  async refreshReportTableData(index:any) {
    this._toastr.info(('Refreshing the table'),'Please wait ' , { disableTimeOut: true });
    //Bind direct id from dropdown objects
    let tab = Object.assign({}, this.tabList[this.tabList.findIndex(x => x.active)]);
    let response = await this._apiService.makeAPICallAsync({
      action: 'getReportDataTable',


      isRefresh:true,
      reportstructure:this.tabList[index].reportStructure
    });

    if (response && response.status) {
      console.log('getReportDataTable response', response.data);
      this.tabList[index].tableHeader = response.data.header;
      this.tabList[index].tableRows = response.data.data;
      console.log('getReportDataTable', this.parameterList);

    } else {
     
    }
    this._toastr.clear();
    console.log('getReportDataTable response', response);
  }

  // Handle timer off or on
  async realTimeCountdownHandler(index:number){
    if(this.tabList[index].timer===''){
      this.secondsTimer(index,this.tabList[index].reportbuilder.duration)
      this.realTimeCountDownTimer(index)
    }else{
      this.clearRealtimeTimer(index);
    }
   }

  //Handle countdown timer and refresh the table automatically for realtime
  async realTimeCountDownTimer(index:number){
    //Outer interval timer to execute based on the report duration 
    this.tabList[index].durationTimer= setInterval(() => {
      let duration=this.tabList[index].reportbuilder.duration;
      clearInterval(this.tabList[index].secondsTimer);

       this.secondsTimer(index,duration);

      //Refresh the report for every report duration interval
      this.refreshReportTableData(index);
    }, this.tabList[index].reportbuilder.duration);
  }

  async secondsTimer(index:number,duration:any){
  //Inner Interval timer to change timer value for every second
  this.tabList[index].secondsTimer=setInterval(()=>{
  this.tabList[index].timer=String(Math.floor(duration / 60000)).padStart(2, '0') 
 + ':' + String(Math.floor((duration % 60000) / 1000)).padStart(2, '0');
  if(duration>=1000)
  duration=duration-1000;

  },1000);
  }

  reportPageChanged(event:any){
    console.log('event',event);
  }


  //Back to overview page from report page
  ReportToOverviewPage() {
    this.clearRealtimeTimer(this.tabList.findIndex(x => x.active));
    this.tabList[this.tabList.findIndex(x => x.active)].showReportTable = false;
  }


  //Handle the folder drill down
  onFolderClicked(folder: any) {
    const path = this.sharedService.getDirectoryPath().value;
    path.push({ name: folder.name, id: folder.id });
    this.sharedService.setDirectoryPath(path);
  }


  // Method to handle adding new tab or open existing tab when the builder is clicked
  onReportClicked(report: any, checkExistTab: boolean, showModal: boolean) {

    if (showModal)
      this.mapReportEditData(report);

    const newTab = {
      index:this.tabList.length,
      title: this.report.name,
      content: this.report.description,
      disabled: false,
      removable: true,
      data: this.report,
      id: this.report.id,
      type: 'file',
      parameters: [],
      timer:'',
      durationTimer:null,
      secondsTimer: null,
      pagination:{
        first:0,
        pagesize:environment.reportPageSize
      }
    };

    let index: any;
    if (checkExistTab && this.tabList.some(x => x.id == this.report.id && x.type === 'file')) {
      if (showModal) {
        this.customFieldModalRef = this.modalService.show(this.tabConfirmationTemplate, {
          backdrop: 'static',
          keyboard: false
        });
        return;
      }
      else {
        this.customFieldModalRef?.hide();
      }
      index = this.tabList.findIndex(x => x.id == this.report.id) + 1;
    }
    else {
      this.tabList.push(newTab);
      index = this.tabList?.length;
      this.customFieldModalRef?.hide();
    }

    this.showTabPanel = true;
    setTimeout(() => {
      this.selectTab(index)
    }, 0)
  }

  //Select the active tab
  selectTab(tabId: number) {
    if (this.staticTabs?.tabs[tabId]) {
      this.staticTabs.tabs[tabId].active = true;
    }
  }

  //Remove tab from tab list
  removeTab(tab: ReportTab): void {
    let index=this.tabList.indexOf(tab);
    this.clearRealtimeTimer(index);
    this.tabList.splice(index, 1);
    console.log('Remove Tab handler');
  }



}

