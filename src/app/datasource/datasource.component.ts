// Default import
import { Component, ElementRef, OnInit, Renderer2, ViewChild, OnDestroy, NgZone, TemplateRef } from '@angular/core';
import { ChangeDetectorRef } from '@angular/core';
import { ApiService } from '../services/api.service';
import { ToastrService } from 'ngx-toastr';
import { MessageService } from 'primeng/api';
import { MenuItem } from 'primeng/api';
// Shared service import
import { TicketstepperService } from '../services/ticketstepper.service';
import { SharedService } from '../services/shared.service';
import { FormBuilder, Validators } from '@angular/forms';
import { environment } from 'src/assets/environments/environment';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { DataSource } from '../model/common.model';


@Component({
  selector: 'app-datasource',
  templateUrl: './datasource.component.html',
  styleUrls: ['./datasource.component.scss'],
})

export class DatasourceComponent {
  //Children declarations
  @ViewChild('open', { read: ElementRef }) closeRef!: ElementRef<any>;
  @ViewChild('close', { read: ElementRef }) modelRef!: ElementRef<any>;

  addEditDatasourceModalRef!: BsModalRef;
  deleteDataSoureModelRef!: BsModalRef;


  // String type variables
  searchKey: string = '';
  spinnerComponent:string='';
  checked: boolean = false;
  sqlTypeSearchValue: string | undefined = '';
  sqlTypeValue = 'pgsql';
  datasourcePriTestResult = 'none';
  datasourceSecTestResult = 'none';

  //Integer variables
  currentStep = 1;
  numSteps = 2;
  index = 0;
  active = 0;
  private activeTabIndex = -1;


  //Boolean variables
  isEdit: boolean = false;
  popupPrimarySelected: boolean = true;
  configPage: boolean = false;
  basicDetailspage: boolean = false;
  spinnerTriggered: boolean = false;
  basicFieldsValid = false;
  configFieldValid = false;
  showErrors = false;
  showPopupLoader = false;
  datasourceStatusToggled = false;

  // List type variables
  dataSourceList: any[] = [];
  masterDataSource: any[] = [];
  sqlLanguageTypes: any[] = environment.sqlLanguageTypes;
  formattedDate: any;
  dateString: string = "2024-03-10T 10:10:10"; // Note: Use 'T' instead of space for ISO 8601 compliance
  dateObject = new Date(this.dateString);
  borderprimaryActive: boolean = false;
  datasourceitems: any
  datasourceactiveItem: any | undefined;
  datasource: any;




  items: MenuItem[] | undefined;


  constructor(
    private _apiService: ApiService,
    private _toastr: ToastrService,
    public _sharedService: SharedService,
    public messageService: MessageService, private renderer: Renderer2, private elRef: ElementRef,
    public ticketService: TicketstepperService, private _formBuilder: FormBuilder,
    private modalService: BsModalService,
  ) { }



  ngOnInit() {
    this.clear();
  }

  onRadioChange(key: any) {

  }



  filterTypes(event: any, options: any[]) {

    this.sqlLanguageTypes = environment.sqlLanguageTypes.filter(option =>
      option.label.toLowerCase().includes(this.sqlTypeSearchValue ?? '')
    );
  }

  nextStep(): void {
    this.showErrors = true;
    this.fieldValidation('basic');
    if (!this.basicFieldsValid) {
      this._toastr.warning('Please Fill the required fields');
    }
    else {
      this.showErrors = false;
      this.currentStep++;
      if (this.currentStep > this.numSteps) {
        this.currentStep = 1;
      }
      const steps = this.elRef.nativeElement.querySelectorAll('.step');

      steps.forEach((step: any, index: any) => {
        const stepNum = index + 1;
        if (stepNum === this.currentStep) {
          this.renderer.addClass(step, 'editing');
        } else {
          this.renderer.removeClass(step, 'editing');
        }
        if (stepNum < this.currentStep) {
          this.renderer.addClass(step, 'done');
        } else {
          this.renderer.removeClass(step, 'done');
        }
      });
      this.basicDetailspage = false;
      this.configPage = true;
    }
  }

  backStep(step: any) {
    if (this.currentStep > 1) {
      this.currentStep--;
    } else {
      this.currentStep = this.numSteps;
    }
    const steps = this.elRef.nativeElement.querySelectorAll('.step');

    steps.forEach((step: any, index: any) => {
      const stepNum = index + 1;
      if (stepNum === this.currentStep) {
        this.renderer.addClass(step, 'editing');
      } else {
        this.renderer.removeClass(step, 'editing');
      }
      if (stepNum < this.currentStep) {
        this.renderer.addClass(step, 'done');
      } else {
        this.renderer.removeClass(step, 'done');
      }
    });
    this.basicDetailspage = true;
    this.configPage = false;
  }

  // Method to reset/clear all fields.
  clear = () => {
    this.datasource = new DataSource();
    this.dataSourceList = [];
    this.masterDataSource = [];
    this.searchKey = '';
    this.sqlTypeSearchValue = '';
    this._sharedService.selectedSortType = 'createddate';
    this.sqlLanguageTypes = environment.sqlLanguageTypes;
    this.getAllDatasource();
  };

  // Method to call when search icon is pressed.
  search() {
    this.dataSourceList = [];
    if (this.searchKey === '') {
      this.dataSourceList = this.masterDataSource;
    }
    else {
      this.dataSourceList = this.masterDataSource.filter(
        x => x.name.includes(this.searchKey)
      );
    }
  }

  // On change in sorting option.
  onSortChange = (field: string, order: string) => {
    this.dataSourceList = this._sharedService.sorting(
      this.dataSourceList,
      field,
      order
    );
  };



  fieldValidation(stepper: string) {
    if (stepper === 'basic') {
      if (this.datasource.name != '' && this.datasource.type != '') {
        this.basicFieldsValid = true;
        return;
      }
      this.basicFieldsValid = false;
      return;
    }
    else if (stepper === 'config') {
      if (this.datasource.primaryHost != '' && this.datasource.primaryPort != '' && this.datasource.primaryUsername != '' && this.datasource.primaryPassword != '' && this.datasource.primaryDatabaseName != ''
        && (!this.datasource.failsafe || (this.datasource.secondaryHost != '' && this.datasource.secondaryPort != '' && this.datasource.secondaryUsername != '' && this.datasource.secondaryPassword != '' && this.datasource.secondaryDatabaseName != ''))
      ) {
        this.configFieldValid = true;
        return;
      }
      this.configFieldValid = false;
      return;
    }
  }

  // Method to Get All Datasource from Database.
  getAllDatasource = () => {
    this._apiService
      .makeAPICall({
        action: 'getAllDataSource',
        searchKey: this.searchKey,
        loader: 'MainPageLoader'
      })
      .subscribe((response) => {
        if (response && response.status) {
          if (response.data.length > 0) {
            for (let i = 0; i < response.data.length; i++) {
              response.data[i].showPrimary = response.data[i]
                ?.connecteddatasource == 'secondary' ? false : true;
              response.data[i].showTestSpinner = false;
              this.masterDataSource.push(response.data[i]);
              this.dataSourceList = this.masterDataSource;
              console.log("data source list", this.dataSourceList);
            }
          } else {
          }
        }
      });
  };

  toggleDataSourceStatus(data: any) {
    this.datasourcePriTestResult = "true";
    this.datasourceSecTestResult = "true";
    this.datasourceStatusToggled = true;
    this.mapEditedData(data);
    this.updateDataSourceStatus();
  }

  //update status
  updateDataSourceStatus(){
    this._toastr.info(('Please wait '), 'Updating Status', { disableTimeOut: true });

    //For dropdown values, Need to set value from dropdown object
    this.datasource.type = this.datasource.type.value;
    this._apiService
      .makeAPICall(this.datasource)
      .subscribe((response) => {
        this._toastr.clear();

        if (response.status && response.data.message.split('-')[1] === 'true') {
          this._toastr.success(('Datasource updated successfully'));
          this.clear();
        }
        else {
          this._toastr.error(( 'Update Error'), response.data.message.split('-')[0], { disableTimeOut: true });
        }
        this.spinnerComponent='';
        console.log('response', response);
      });
  }



  // Insert Datasource if type is 'add' else updates the existing report builder.
  // Clone existing Datasource and create a new one
  storeDataSource() {
    this.spinnerComponent='submitAddEditDataSource';
    this.showErrors = true;
    let allowSaveInBasicPage = false;
    
    //Condition - check whether the basic fields are valid or not in edit popup
    if (this.isEdit && this.basicDetailspage) {
      this.fieldValidation('basic');
      if (!this.basicFieldsValid) {
        this._toastr.info('Please Fill the required fields');
        return;
        this.spinnerComponent='';
      }
      allowSaveInBasicPage = true;
    }


    //Condition - check whether the config fields are valid or not in add/edit popup
    if (!allowSaveInBasicPage) {
      this.fieldValidation('config');
      if (!this.configFieldValid) {
        this._toastr.info('Please Fill the required fields');
        this.spinnerComponent='';
        return;
      }
      else {
        if (this.datasourcePriTestResult === 'none') {
          this._toastr.info('Please test the primary connection');
          this.spinnerComponent='';
          return;
        }
        else if (this.datasource.failsafe && this.datasourcePriTestResult === 'none') {
          this._toastr.info('please test the secondary connection');
          this.spinnerComponent='';
          return;
        }
      }
    }

    //For dropdown values, Need to set value from dropdown object
    this.datasource.type = this.datasource.type.value;

    //ADD - if both basic and config fields are validated , Store it
    //EDIT -if either basic or config fields are validdated, save it
    this._apiService
      .makeAPICall(this.datasource)
      .subscribe((response) => {

        if (response.status && response.data.message.split('-')[1] === 'true') {
          this._toastr.success(('Datasource ' + (this.isEdit ? 'updated' : 'inserted' + ' successfully')));
          if (!this.datasourceStatusToggled)
            this.addEditDatasourceModalRef?.hide();
          this.clear();
        }
        else {
          this._toastr.error((this.isEdit ? 'Update Error' : 'Insert Error'), response.data.message.split('-')[0], { disableTimeOut: true });
        }
        this.spinnerComponent='';
        console.log('response', response);
      });
  };

  //Open or hide the delete popup
  toggleDeleteModal(action: string, template: TemplateRef<any>) {
    if (action === 'open')
      this.deleteDataSoureModelRef = this.modalService.show(template, {
        backdrop: 'static',
        keyboard: false,
      });
    else if (action === 'close')
      this.deleteDataSoureModelRef?.hide();
  }

  // Method to Delete Datasource from Database.
  async deleteDatasource() {
    this.spinnerComponent = 'deleteDataSource';
    this.datasource.action = 'deleteDataSource';
    let response = await this._apiService.makeAPICallAsync(this.datasource);
    console.log('response', response);
    if (response.status && response.data) {
      this._toastr.success(('Data Source deleted successfully'));
      this.deleteDataSoureModelRef?.hide();
      this.clear();
    }
    else {
      this._toastr.error(('Delete Error - '), response.message, { disableTimeOut: true });
    }
    this.spinnerComponent = '';
  }

  constructDataSourceEvent(type: string) {
    return {
      "action": "testDBConnection",
      "host": type === 'primary' ? this.datasource.primaryHost : this.datasource.secondaryHost,
      "port": type === 'primary' ? this.datasource.primaryPort : this.datasource.secondaryPort,
      "username": type === 'primary' ? this.datasource.primaryUsername : this.datasource.secondaryUsername,
      "password": type === 'primary' ? this.datasource.primaryPassword : this.datasource.secondaryPassword,
      "dbClusterIdentifier": type === 'primary' ? this.datasource.primaryDatabaseName : this.datasource.secondaryDatabaseName
    }
  }

  async testAllHostConnection(index: number) {

    this.dataSourceList[index].showTestSpinner = true;
    this.mapEditedData(this.dataSourceList[index]);
    console.log('datasource', this.datasource);
    console.log('datasourcelist', this.dataSourceList);
    let priResponse = await this._apiService
      .makeAPICallAsync(this.constructDataSourceEvent('primary'));
    if (priResponse.data) {
      this.dataSourceList[index].testresult = priResponse;
      this.dataSourceList[index].connecteddatasource = 'primary';
      this._toastr.success('Connected to Primary data source');
    }
    else {
      let secResponse = await this._apiService
        .makeAPICallAsync(this.constructDataSourceEvent('secondary'));
      if (secResponse.data) {
        this.dataSourceList[index].testresult = secResponse;
        this.dataSourceList[index].connecteddatasource = 'secondary';
        this._toastr.success('Connected to Secondary data source');
      }
      else {
        this.dataSourceList[index].connecteddatasource = '';
        this._toastr.error('Data source is invalid or down');
      }

    }
    this.dataSourceList[index].showTestSpinner = false;
  }

  // Method to Test Primary or Secondary Host Connection.
  async testSingleHostConnection() {


    if (this.popupPrimarySelected) {
      if (this.datasource.primaryHost != '' && this.datasource.primaryPort != '' && this.datasource.primaryUsername != '' && this.datasource.primaryPassword != '' && this.datasource.primaryDatabaseName != '') {

        this.datasourcePriTestResult = 'testing';
      }
      else {

        this._toastr.warning('Please Fill the Primary fields');
        return;
      }
    }
    else {
      if (this.datasource.failsafe && (this.datasource.secondaryHost != '' && this.datasource.secondaryPort != '' && this.datasource.secondaryUsername != '' && this.datasource.secondaryPassword != '' && this.datasource.secondaryDatabaseName != '')) {

        this.datasourcePriTestResult = 'testing';
      }
      else {
        this._toastr.warning('Please Fill the Secondary fields');
        return;
      }
    }

    let event = this.constructDataSourceEvent(this.popupPrimarySelected ? 'primary' : 'secondary');
    let response = await this._apiService
      .makeAPICallAsync(event);
    console.log('testdbconnection', response);
      if (this.popupPrimarySelected) {
        this.datasourcePriTestResult = response.data === true ? 'true' : 'false';
      }
      else {
        this.datasourceSecTestResult = response.data === true ? 'true' : 'false';
      }
  };


  onTabSelected(event: any) {
    this.activeTabIndex = event.index;
  }

  isActive(index: number): boolean {
    this.borderprimaryActive = true;
    this.activeTabIndex === index;
    return true;
  }

  getStepperClasses(index: number): object {
    return {
      'bg-primary border-primary': index <= this.active,
      'surface-border': index > this.active
    };
  }

  openAddEditPopup(action: string, template: TemplateRef<any>) {
    if (action === 'open') {
      this.resetAddEditPopup();
      if (this.isEdit) {
        this.currentStep = 1;
        const steps = this.elRef.nativeElement.querySelectorAll('.step');

        steps.forEach((step: any, index: any) => {
          if (index === 0) {
            this.renderer.addClass(step, 'editing');
            this.renderer.removeClass(step, 'done');
          } else {
            this.renderer.addClass(step, 'done');
            this.renderer.removeClass(step, 'editing');
          }
        });

        console.log('edited data source', this.datasource);

      }
      else {
        this.datasource =new DataSource();
        console.log('Add data source', this.datasource);
      }
      this.addEditDatasourceModalRef = this.modalService.show(template, {
        backdrop: 'static',
        keyboard: false,
      });
    }
    else {
      this.addEditDatasourceModalRef?.hide();
    }
  }

  resetAddEditPopup() {
    this.datasourcePriTestResult = 'none';
    this.datasourceSecTestResult = 'none';
    this.showErrors = false;
    this.basicFieldsValid = false;
    this.configFieldValid = false;
    this.popupPrimarySelected = true;
    this.currentStep = 2;
    this.basicDetailspage = false;
    this.configPage = false;
    this.sqlTypeSearchValue = '';
    this.datasourceStatusToggled = false;
    this.backStep(1);
  }


  captureactive(value: any) {
    console.log("what is the value:", value)
  }

  TestConnection() {
    this.spinnerTriggered = true;
    setTimeout(() => {
      this.spinnerTriggered = false;

    }, 3000)
  }

  mapEditedData(editedData: any) {
    this.datasource = new DataSource();
    this.datasource = {
      action: 'editDataSource',
      id: editedData.id,
      name: editedData.name,
      type: { label: editedData.type, value: editedData.type },
      primaryHost: editedData.prihost,
      primaryPort: editedData.priport,
      primaryDatabaseName: editedData.pridatabasename,
      primaryUsername: editedData.priusername,
      primaryPassword: editedData.pripassword,
      secondaryHost: editedData.sechost,
      secondaryPort: editedData.secport,
      secondaryDatabaseName: editedData.secdatabasename,
      secondaryUsername: editedData.secusername,
      secondaryPassword: editedData.secpassword,
      isEnable: editedData.enabled,
      timezone: editedData.timezone,
      description: editedData.description,
      failsafe: editedData.failsafe,
      status: editedData.status, // 
      userid: 1 // 
    };
  }

  clearDropdown(event:any){
    console.log('dropdown event',event);
  }
}
