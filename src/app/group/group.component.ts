import {
  Component,
  ElementRef,
  OnInit,
  Renderer2,
  ViewChild,
  OnDestroy,
  NgZone,
  TemplateRef,
} from "@angular/core";
import { ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import { DropdownFilterOptions } from "primeng/dropdown";
import { ApiService } from "../services/api.service";
import { ToastrService } from "ngx-toastr";
import { BsModalService } from "ngx-bootstrap/modal";
import { SharedService } from "../services/shared.service";
import { environment } from 'src/assets/environments/environment';
import { object } from "@amcharts/amcharts4/core";

interface user {
  name?: string;
}

@Component({
  selector: "app-group",
  templateUrl: "./group.component.html",
  styleUrls: ["./group.component.scss"],
})
export class GroupComponent implements OnInit {
  @ViewChild("fullscreenDiv") fullscreenDiv: ElementRef | undefined;
  @ViewChild("fullScreen", { read: ElementRef }) divRef!: ElementRef<any>;
  @ViewChild("open", { read: ElementRef }) closeRef!: ElementRef<any>;
  @ViewChild("close", { read: ElementRef }) modelRef!: ElementRef<any>;
  searchValue!: any;

  Users: user[] | undefined;

  selectedUser: user | any;
  filterValue: string | undefined = "";
  filteredGroups:any[]=[];
  groupListResponse: any[] = [];
  groupForm: FormGroup | any;
  isEdit: boolean = false;
  selectedGroup:any;
  moduleList:any;
  filteredGroupUsersPermission:any=[];

  first = 0;

  rows = 10;

 
  constructor(
    public _apiService: ApiService,
    public _toastr: ToastrService,
    private _form: FormBuilder,private modalService: BsModalService,public _sharedService: SharedService
  ) {}

  // Method to get the Groups details
  getGroupsList = () => {
    this._apiService
      .makeAPICall({
        action: "getGroups",
        
      })
      .subscribe((response) => {
        const group = JSON.parse(response.body);
        this.filteredGroups = group.groups.map((x: any) => ({
          groupName: x.groupname,
          name:x.groupname,
          description: x.description,
          createddate: x.creationDate,
          lastmodifieddate: x.lastmodifieddate,
          users: x.users ? x.users.map((user:any) => ({
            email: user.email,
          })) : [],
        }));
        this.groupListResponse = this.filteredGroups
       
      });
  };


//     this._apiService
//       .makeAPICall({
//         action: "getGroupList",
//         type: "getGroupUserList",
//         groupname: group.groupName,
//       })
//       .subscribe((response) => {
        
//         const jsonObject = JSON.parse(response.body);

//         const users = jsonObject.Users;
//         const userEmails = users.map((user: any) => {
//             const emailAttribute = user.Attributes.find((attr: any) => attr.Name === "email");
//             return emailAttribute ? emailAttribute.Value : null;
//           });
//           this.groupForm.patchValue({
//             users: userEmails,
//           });
           
//         });
      
//   };
  // Method to get the Users name to Load on Dropdown
  getListUsers = () => {
    this._apiService
      .makeAPICall({
        action: "getUsersList",
      })
      .subscribe((response) => {
        const User = JSON.parse(response.body);

        this.Users = User.filter((user: any) => 
          user.Attributes.find((attr:any) => attr.Name === 'custom:isGroup'&& attr.Value === 'N' )
        ).map((user: any) => ({
          email: user.Attributes.find((attr: any) => attr.Name === "email")
            .Value,
        }));
        this. filteredGroupUsersPermission = User.map((user:any) => ({
          email: user.Attributes.find((attr: any) => attr.Name === "email").Value,
          grouppermission: user.Attributes.find((attr:any) => attr.Name === 'custom:grouppermission') ,
        }));
        
      });
  };
 // Method to Submit the Newly Created group & Updated the Existing group
  onSubmit() {

    if(!this.isEdit){
    const groupobj=this.groupForm.value;
    this._apiService.makeAPICall({
        action:"createGroups",
        groupname:groupobj.groupname,
        description:groupobj.description,
        users:groupobj.users
    }).subscribe((response)=>{
        if(response.statusCode == 200){
            this._toastr.success("Group Created");
            this.getGroupsList();
        }
        else{
            this._toastr.error(JSON.parse(response.body).message);
        }
    })    
}
else{
     
    const groupobj=this.groupForm.value;
    this._apiService.makeAPICall({
        action:"updateGroups",
        groupname:groupobj.groupname,
        description:groupobj.description,
        users:groupobj.users

    }).subscribe((response)=>{
        if(response.statusCode == 200){
            this._toastr.success("Group Updated");
            this.getGroupsList();
        }
        else{
            this._toastr.error(JSON.parse(response.body).message);
        }
    })
    }
   }

   // Method to Delete the Group 
   deleteGroups=(groupname:any)=>{
      this._apiService.makeAPICall({
        action:"deleteGroups",
        groupname:groupname
      }).subscribe((response)=>{
        if(response.statusCode == 200){
        this._toastr.error("Group Deleted Successfully");
        this.modalService.hide();
        this.getGroupsList();
        }
      })
   }
  // Method to Call When the Reload Button Clicked
   clear(){
    this.groupListResponse = [];
    this.filteredGroups=[];
    this.searchValue = '';
    this.getGroupsList();
  }

  openAddPopup() {
    this.groupForm.reset();
  }

  openEditPopup(group: any) {
    if (this.isEdit) {
      this.groupForm.patchValue({
        groupname: group.groupName, 
        users: group.users,
        description: group.description,
      });
    }
    
  }

  toggleDeleteModal(action: string, template: TemplateRef<any>) {
    if (action === 'open')
      this.modalService.show(template);
    else if (action === 'close')
      this.modalService.hide();
  }

  ngOnInit(): void {
    this.getGroupsList();
    this.getListUsers();

    this.groupForm = this._form.group({
      groupname: ["", Validators.required],
      users: ["", Validators.required],
      description: ["", Validators.required],
    });

    this.moduleList=environment.modules;
    this.moduleList.forEach((x: any)=>{
      x.add=false;
      x.view=false;
      x.delete=false;
      x.edit=false;
    })
  }

  next() {
    this.first = this.first + this.rows;
  }

  prev() {
    this.first = this.first - this.rows;
  }

  reset() {
    this.first = 0;
  }

  openModal() {
    this.modelRef.nativeElement.click();
  }
  closeModal() {
    this.closeRef.nativeElement.click();
  }

  pageChange(event: any) {
    this.first = event.first;
    this.rows = event.rows;
  }

  isLastPage(): boolean {
    return this.groupListResponse
      ? this.first === this.groupListResponse.length - this.rows
      : true;
  }

  isFirstPage(): boolean {
    return this.groupListResponse ? this.first === 0 : true;
  }

  onRadioChange(key: any) {}

  public dt = {
    filterGlobal(value: any, filterType: string): void {
      console.log("Filtering globally", value);
      // Implement your global filtering logic here
    },
  };

  resetFunction(options: DropdownFilterOptions) {
    if (options) {
      // options!.reset();
    }
    this.filterValue = "";
  }

  customFilterFunction(event: KeyboardEvent, options: DropdownFilterOptions) {
    // options.filter(event);
  }

  // Method to call when search icon is pressed.
  search() {
    if (this.searchValue.trim() === '') {
      
      this.groupListResponse = this.filteredGroups;

      
    } else {
      
      this.groupListResponse = this.filteredGroups.filter(
        ( x: { groupName: string; description: string;  }) => x.groupName.toLowerCase().includes(this.searchValue.toLowerCase()) ||
        x.description.toLowerCase().includes(this.searchValue.toLowerCase()) 
       
    );
      
    }
  }
}
