import { Injectable } from '@angular/core';
import { SharedService } from './shared.service';
 
@Injectable({
  providedIn: 'root',
})
export class InterceptorService {
  constructor(private _sharedService: SharedService){

  }
  private loading = false;
  private mainPageLoading = false;
 
  showLoader(): void {
    this.loading = true;
    this._sharedService.showLoader=true;
    // Implement logic to show the loader (e.g., set a flag, show a spinner component, etc.)
  }
 
  hideLoader(): void {
    this._sharedService.showLoader=false;
    this.loading = false;
    // Implement logic to hide the loader (e.g., reset the flag, hide the spinner component, etc.)
  }

  showMainPageLoader(): void {
    this.mainPageLoading = true;
    this._sharedService.showMainPageLoader=true;
    // Implement logic to show the loader (e.g., set a flag, show a spinner component, etc.)
  }
 
  hideMainPageLoader(): void {
    this._sharedService.showMainPageLoader=false;
    this.mainPageLoading = false;
    // Implement logic to hide the loader (e.g., reset the flag, hide the spinner component, etc.)
  }

  isMainPageLoading():boolean{
    return this.mainPageLoading;
  }
 
  isLoading(): boolean {
    return this.loading;
  }
}
 