import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthGuardService {
  private loggedInSource = new BehaviorSubject<boolean>(false);
  currentLoggedIn = this.loggedInSource.asObservable();
  constructor(private router: Router) {}

  changeLoginStatus(status: boolean) {
    this.loggedInSource.next(status);
  }

  getCurrentLoginStatus(): boolean {
    return this.loggedInSource.value;
  }
}
