import { PrimeNGConfig } from 'primeng/api';
import { Component,ElementRef,OnInit, Renderer2, ViewChild,OnDestroy, NgZone  } from '@angular/core';
import { ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup } from '@angular/forms';
import { DropdownFilterOptions } from 'primeng/dropdown';
import { ApiService } from '../services/api.service';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/assets/environments/environment';
import { object } from '@amcharts/amcharts4/core';
@Component({
  selector: 'app-permission',
  templateUrl: './permission.component.html',
  styleUrls: ['./permission.component.scss']
})
export class PermissionComponent {
  @ViewChild('fullscreenDiv') fullscreenDiv: ElementRef | undefined;
  @ViewChild('fullScreen',{read:ElementRef}) divRef!: ElementRef<any>;
 @ViewChild('openUsers',{read: ElementRef})closeusersRef!:ElementRef<any>;
 @ViewChild('closeUsers',{read: ElementRef})modelusersRef!:ElementRef<any>;
 @ViewChild('openGroups',{read: ElementRef})closegroupsRef!:ElementRef<any>;
 @ViewChild('closeGroups',{read: ElementRef})modelgroupsRef!:ElementRef<any>;
  isButtonRounded: boolean = false;
  activeIndex: number = 0;
  filteredNames:any=[];
  filteredGroupsNames:any=[];
  filteredpermission:any= [];
 selectRow :boolean = false;
 selectAllRow : boolean = false;
 currentUsers :any ='';
 currentGroupName:any='';
 groupPermission:any;
 userGroupMapping :any= {};
 searchValue:any='';
 moduleList:any;
 isGroupUpdate:boolean=false;
 filteredGroupUsersPermission:any=[];

 
// selectedProducts = true;

  files: File[] = [];

  totalSize: number = 0;

  totalSizePercent: number = 0;

  userItems :any

  groupItems:any

  constructor(private config: PrimeNGConfig,public _apiService:ApiService, public _toastr : ToastrService) {}

  visible: boolean = false;


  ngOnInit(): void {
    this.getUsersName();
    this.getGroupsName();
    

    this.moduleList=environment.modules;
    this.moduleList.forEach((x: any)=>{
      x.add=false;
      x.view=false;
      x.delete=false;
      x.edit=false;
      x.p='0000';
    })
    
    
  }
  // Method to Get the UserList to show on UsersPermission
  getUsersName = ()=>{
    this._apiService.makeAPICall({
        action:"getUsersList",
    }).subscribe((response)=>{
        
        const Users=JSON.parse(response.body);
        console.log("getusers",response)
          this.filteredNames = Users.filter((user: any) => 
            user.Attributes.find((attr:any) => attr.Name === 'custom:isGroup'&& attr.Value === 'N' )
          ).map((user: any) => ({
            email: user.Attributes.find((attr: any) => attr.Name === 'email').Value,
          }));
          console.log(this.filteredNames)
          this.userItems= this.filteredNames;
          
          
    });
    
  }
    // Method to Get the Groupnames to show on the Groups Permission         
    getGroupsName = () => {
      this._apiService
        .makeAPICall({
          action: "getGroups",
          
        })
        .subscribe((response) => {
          const group = JSON.parse(response.body);
          this.filteredGroupsNames = group.groups.map((x: any) => ({
            groupname: x.groupname,
            name:x.groupname,
            description: x.description,
            createddate: x.creationDate,
            lastmodifieddate: x.lastmodifieddate,
            users: x.users ? x.users.map((user:any) => ({
              email: user.email,
            })) : [],
          }));
          this.groupItems=this.filteredGroupsNames;
          console.log(this.groupItems)
        });
    };
    
    // Method to map the Custom:Permission Attributes JSON to User and Group Permissions
     mapPermissions(p: string): Record<string, boolean> {
      const permissions = ['add', 'edit', 'view', 'delete'];
      const permissionStatus: Record<string, boolean> = {};
    
      for (let i = 0; i < permissions.length; i++) {
        permissionStatus[permissions[i]] = p[i] === '1';
      }
    
      return permissionStatus;
    }
  // Method to get the Users Permission 
  getpermission=(item:any)=>{
   this.currentUsers=item;
    this._apiService.makeAPICall({
      action:"getUsers",
      username:item
    }).subscribe((response)=>{
      console.log("permission",response)
      const permission=JSON.parse(response.body);
      const permissionAttr = permission.UserAttributes.find((attr:any) => attr.Name === 'custom:permission');
      if(permissionAttr){
        const permissionObj= JSON.parse(permissionAttr.Value)
        this.filteredpermission=permissionObj;

        console.log('permissionobj',permissionObj)
      permissionObj.forEach((module:any) => {
        const permissions = this. mapPermissions(module.p);
        const moduleIndex = this.moduleList.findIndex((m :any)=> m.id === module.id);
        if (moduleIndex !== -1) {
         this. moduleList[moduleIndex]['add'] = permissions['add'];
         this. moduleList[moduleIndex]['edit'] = permissions['edit'];
         this. moduleList[moduleIndex]['view'] = permissions['view'];
         this. moduleList[moduleIndex]['delete'] = permissions['delete'];
        }
      });
      console.log('module permission',this.moduleList);
    }
      else{
        this.moduleList.forEach((x: any)=>{
          x.add=false;
          x.view=false;
          x.delete=false;
          x.edit=false;
          x.p='0000';
        })
        
      }
    });
  }
  // Method to Update the Users and Groups Permission
  updatePermission =(isGroupUpdate:any)=>{
    console.log(this.moduleList);
    const mergedItems = this.moduleList.map((item:any) => {
      const binaryP = ['add', 'edit', 'view', 'delete'].map(key => key in item ? (item[key] ? '1' : '0') : '0').join('');
      return {...item, p: binaryP};
  });
  console.log(mergedItems)
    const result =mergedItems.map((x:any)=>({ id: x.id, p: x.p}))
    console.log(result)
   if(isGroupUpdate){
    this._apiService.makeAPICall({
      action: "updateUsers",
      type: "updatePermission",
      username:this.currentUsers,
      jsonstring:  JSON.stringify(result)
     }).subscribe((response)=>{
      
      if(response.statusCode == 200 )
        {
          this._toastr.success("Permission Updated");
        }
        
     })
   }
   else{
    this._apiService.makeAPICall({
      action: "updateUsers",
      type: "updatePermission",
      username:this.currentGroupName+"@Servion.com",
      jsonstring:  JSON.stringify(result)
     }).subscribe((response)=>{
      
      if(response.statusCode == 200 )
        {
          this._toastr.success(" Group Permission Updated");
        }
        
     })
   }
  }
  // Method to get the Groups Permission
  getGroupPermission(groupname:any){
    this.currentGroupName=groupname;
    this._apiService.makeAPICall({
      action:"getUsers",
      username:groupname+"@Servion.com",
    }).subscribe((response)=>{
      console.log("permission",response)
      const permission=JSON.parse(response.body);
      const permissionAttr = permission.UserAttributes.find((attr:any) => attr.Name === 'custom:permission');
      if(permissionAttr){
        const permissionObj= JSON.parse(permissionAttr.Value)
        this.filteredpermission=permissionObj;

        console.log('permissionobj',permissionObj)
      permissionObj.forEach((module:any) => {
        const permissions = this. mapPermissions(module.p);
        const moduleIndex = this.moduleList.findIndex((m :any)=> m.id === module.id);
        if (moduleIndex !== -1) {
         this. moduleList[moduleIndex]['add'] = permissions['add'];
         this. moduleList[moduleIndex]['edit'] = permissions['edit'];
         this. moduleList[moduleIndex]['view'] = permissions['view'];
         this. moduleList[moduleIndex]['delete'] = permissions['delete'];
        }
      });
      console.log('module permission',this.moduleList);
    }
      else{
        this.moduleList.forEach((x: any)=>{
          x.add=false;
          x.view=false;
          x.delete=false;
          x.edit=false;
          x.p='0000';
        })
        
      }
    }); 
  }  

  toggleSelectRow(event:any,index:any){

  let isChecked=event.target.checked;
    
      // Check if the index is valid and within the bounds of the filteredpermission array
  if (index >= 0 && index < this.moduleList.length) {
    // Toggle the selectRow flag
    

    // Update the properties of the item at the given index
    this.moduleList[index].add = isChecked;
    this.moduleList[index].edit = isChecked;
    this.moduleList[index].view = isChecked;
    this.moduleList[index].delete =isChecked;
  }
  }
 
  // Method to Toggle the Permission checkbox to select 
 toggleSelectAllRow(){
  if(this.selectAllRow){
    this.moduleList.forEach((item:any) => {
      item.add = this.selectAllRow
      item.edit = this.selectAllRow 
      item.view = this.selectAllRow
      item.delete = this.selectAllRow
    });
    }
    else{
      
        this.moduleList.forEach((item:any) => {
          item.add = this.selectAllRow
          item.edit = this.selectAllRow 
          item.view =this.selectAllRow
          item.delete = this.selectAllRow
        });
      
    }
 }
  

  showDialog() {
    this.visible = true;
  }

  choose(event: any, callback: any) {
    callback();
  }

  onRemoveTemplatingFile(
    event: any,
    file: any,
    removeFileCallback: any,
    index: any
  ) {
    removeFileCallback(event, index);
    this.totalSize -= parseInt(this.formatSize(file.size));
    this.totalSizePercent = this.totalSize / 10;
  }

  onClearTemplatingUpload(clear: any) {
    clear();
    this.totalSize = 0;
    this.totalSizePercent = 0;
  }

  onTemplatedUpload() {
    // this.messageService.add({
    //   severity: 'info',
    //   summary: 'Success',
    //   detail: 'File Uploaded',
    //   life: 3000,
    // });
  }

  onSelectedFiles(event: any) {
    this.files = event.currentFiles as File[];
    this.files.forEach((file) => {
      this.totalSize += parseInt(this.formatSize(file.size));
    });
    this.totalSizePercent = this.totalSize / 10;
  }

  uploadEvent(callback: any) {
    callback();
  }

  formatSize(bytes: any) {
    const k = 1024;
    const dm = 3;
    const sizes = this.config.translation.fileSizeTypes || ['Bytes'];
    if (bytes === 0) {
      return `0 ${sizes[0]}`;
    }

    const i = Math.floor(Math.log(bytes) / Math.log(k));
    const formattedSize = parseFloat((bytes / Math.pow(k, i)).toFixed(dm));

    return `${formattedSize} ${sizes[i]}`;
  }

  tabview(event: any) {
    console.log(event);
    if (event === 1) {
      this.isButtonRounded = true;
    } else {
      this.isButtonRounded = false;
    }
  }
  openuserModal(){
this.closeusersRef.nativeElement.click();
  }
  closeuserModal(){
    this.modelusersRef.nativeElement.click();
  }
  openGroupsModal(){
    this.closegroupsRef.nativeElement.click();
  }
  closeGroupsModal(){
    this.modelgroupsRef.nativeElement.click();
  }

    // Method to call when search icon is pressed.
  search(event:any){
    if(event===0){
    if (this.searchValue.trim() === ''){

      this.userItems=this.filteredNames;
      
    }
    else{
      this.userItems=this.filteredNames.filter((x:{email:string})=>{
      return  x.email.toLowerCase().includes(this.searchValue.toLowerCase())
        
      })
    }
  }
  else
  {
    if (this.searchValue.trim() === ''){

      this.groupItems=this.filteredGroupsNames;
      
    }
    else{
      this.groupItems=this.filteredGroupsNames.filter((x:{groupname:string})=>{
      return  x.groupname.toLowerCase().includes(this.searchValue.toLowerCase())
        
      })
    }

  }
}
}
