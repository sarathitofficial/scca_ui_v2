import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RouteLayoutComponent } from './route-layout.component';

describe('RouteLayoutComponent', () => {
  let component: RouteLayoutComponent;
  let fixture: ComponentFixture<RouteLayoutComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [RouteLayoutComponent]
    });
    fixture = TestBed.createComponent(RouteLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
