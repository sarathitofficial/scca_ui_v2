import { Component, OnInit } from '@angular/core';
import { SharedService } from '../services/shared.service';

@Component({
  selector: 'app-route-layout',
  templateUrl: './route-layout.component.html',
  styleUrls: ['./route-layout.component.scss']
})
export class RouteLayoutComponent implements OnInit  {

  directoryPaths:any;
  title:any={name:'',imagesource:''};
  constructor(public sharedService: SharedService){
    

  }

  
  ngOnInit(): void {
    this.initializeListeners();
  }

  //To listen the shared service details
  initializeListeners(){
    this.sharedService.getDirectoryPath().subscribe(paths => {
      this.directoryPaths = paths;
    });
    this.sharedService.getTitleDetails().subscribe(title=>{
      this.title=title;
    });
  }

  onRouteClicked(r:any){
  const selectedPath=this.sharedService.getDirectoryPath().value;
  if(selectedPath.indexOf(r)!=selectedPath.length-1){
  selectedPath.splice(selectedPath.indexOf(r)+1,selectedPath.length-(selectedPath.indexOf(r)+1));
  this.sharedService.setDirectoryPath(selectedPath);
  }
  }

}
