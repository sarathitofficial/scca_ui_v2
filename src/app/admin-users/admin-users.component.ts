import { Component, ElementRef, OnInit, Renderer2, TemplateRef, ViewChild } from '@angular/core';
import { ApiService } from '../services/api.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { SharedService } from '../services/shared.service';

@Component({
  selector: 'app-admin-users',
  templateUrl: './admin-users.component.html',
  styleUrls: ['./admin-users.component.scss']
})
export class AdminUsersComponent implements OnInit {
    
    @ViewChild('closeFolder',{read: ElementRef})modelFolderRef!:ElementRef<any>;

  selectedUser:any;
  
  
  isEdit:boolean =false;
  basicDetailspage:boolean = false;
  inputParameters:boolean=false;
  configPage:boolean = false;
  checked: boolean = false;
  searchValue:any='';
  usersForm: FormGroup |any;
  userList:any[]=[];
  status:boolean=false;


  constructor(private _apiService: ApiService,private elRef: ElementRef,private renderer: Renderer2,private modalService: BsModalService,private _form:FormBuilder,private _toastr:ToastrService,public _sharedService: SharedService)
  {
    
  }


first = 0;

    rows = 10;
  
  filterUsers:any=[];
  
  ngOnInit(): void {
    this.getUsersList();

    this.usersForm = this._form.group({
        username: ['',Validators.required],
        firstname: ['',Validators.required],
        lastname: ['',Validators.required],
        phonenumber: ['',Validators.required],
        temporarypassword: ['',Validators.required] ,
      });
    
  }
  // Method to get the User 
  getUsersList = ()=>{
    this._apiService.makeAPICall({
        action:"getUsersList",
    }).subscribe((response)=>{
        
        const Users=JSON.parse(response.body);
        this.filterUsers = Users.filter((user: any) => 
          user.Attributes.find((attr:any) => attr.Name === 'custom:isGroup'&& attr.Value === 'N' )
        ).map((user:any) => ({
            email: user.Attributes.find((attr:any) => attr.Name === 'email').Value ,
            name: user.Attributes.find((attr:any) => attr.Name === 'email').Value ,
            firstname: user.Attributes.find((attr:any)=> attr.Name === 'custom:firstname').Value,
            lastname: user.Attributes.find((attr:any)=> attr.Name === 'custom:lastname').Value ,
            phonenumber: user.Attributes.find((attr:any)=> attr.Name === 'custom:phonenumber').Value ,
            status:user.Enabled,
            createddate: user.UserCreateDate,
            lastmodifieddate: user.UserLastModifiedDate
          }));
          
          this.userList=this.filterUsers;
          
    });
    
  }
   // Method to call when the Reload Button clicked
  clear(){
    this.userList = [];
    this.filterUsers=[];
    this.searchValue = '';
    this.getUsersList();
  }


  next() {
    this.first = this.first + this.rows;
}

prev() {
    this.first = this.first - this.rows;
}

reset() {
    this.first = 0;
}

pageChange(event:any) {
    this.first = event.first;
    this.rows = event.rows;
}

isLastPage(): boolean {
    return this.userList ? this.first === this.userList.length - this.rows : true;
}

isFirstPage(): boolean {
    return this.userList ? this.first === 0 : true;
}

  onRadioChange(key:any){

  }

   public dt = {
    filterGlobal(value: any, filterType: string): void {
      console.log('Filtering globally', value);
      // Implement your global filtering logic here
    }
  }

  closeFolderModal(){
    
    this.modelFolderRef.nativeElement.click();
    this.basicDetailspage = false;
    this.inputParameters=false;
    this.configPage=false;
    this.inputParameters=false;
    const element = this.elRef.nativeElement.querySelector(`modal-backdrop.show`);
    if (element) {
    this.renderer.removeClass(element, 'modal-backdrop.show');
    }
   }
   
   openEditPopup(user:any){
    if (this.isEdit){
       this.usersForm.patchValue({
            username: user.email,
            firstname: user.firstname,
            lastname: user.lastname,
            phonenumber: user.phonenumber,
           });     
    }
    
   }
   openAddPopup(){
    this.usersForm.reset();
   }
   //Method to call when the Adding new user Or Updating the Existing User
   onSubmit():void{
    if(this.isEdit){
     this.updateUsers();
    }
    else{
        this.addNewUsers();
       
    }
   }
  // Method to Add the New User
   addNewUsers(): void {
    
    this._apiService.makeAPICall({
        action:'createUsers',
        type:'createUsers',
        username:this.usersForm.controls['username'].value,
        firstname:this.usersForm.controls['firstname'].value,
        lastname:this.usersForm.controls['lastname'].value,
        phonenumber:this.usersForm.controls['phonenumber'].value,
        temporarypassword: this.usersForm.controls['temporarypassword'].value,
        email:this.usersForm.controls['username'].value
    }).subscribe((response)=>{
        
        this._toastr.success("Users Created Successfully");
        this.getUsersList();
    this.closeFolderModal();
    })
    
    
 
  }

  // Method to Update the Existing User
  updateUsers():void{
    this._apiService.makeAPICall({
        action:'updateUsers',
        username:this.usersForm.controls['username'].value,
        firstname:this.usersForm.controls['firstname'].value,
        lastname:this.usersForm.controls['lastname'].value,
        phonenumber:this.usersForm.controls['phonenumber'].value,  
    }).subscribe((response)=>{
        this._toastr.success("User Updated Successfully");
        this.getUsersList();
    this.closeFolderModal();
    })

    
  }

  //Method to Enable Or Disable the User
  enableOrDisableUser=(event:any,username:any)=>{
    if(event.checked){
      this._apiService.makeAPICall({
        action:'updateUsers',
        type:'enableUser',
        username:username
         
    }).subscribe((response)=>{
        this._toastr.success("User enabled Successfully");
        this.getUsersList();
    
    })
    }
    else{
      this._apiService.makeAPICall({
        action:'updateUsers',
        type:'disableUser',
        username:username
         
    }).subscribe((response)=>{
        this._toastr.success("User disabled Successfully");
        this.getUsersList();
    
    })
    }
    
  }

   toggleDeleteModal(action: string, template: TemplateRef<any>) {
    if (action === 'open')
      this.modalService.show(template);
    else if (action === 'close')
      this.modalService.hide();
  }

  // Method to Delete the User
  deleteUsers = (username:any) => {
    
    this._apiService.makeAPICall({
        action:"deleteUsers",
        username:username
    }).subscribe((response) => {
      this._toastr.error("User Deleted Successfully");
      this.modalService.hide();
      this.getUsersList();
    });
  };

  // Method to call when search icon is pressed.
  search() {
    if (this.searchValue.trim() === '') {
      
      this.userList = this.filterUsers;

      
    } else {
      
      this.userList = this.filterUsers.filter(
        ( x: { email: string; firstname: string; lastname: string; }) => x.email.toLowerCase().includes(this.searchValue.toLowerCase()) ||
        x.firstname.toLowerCase().includes(this.searchValue.toLowerCase()) ||
        x.lastname.toLowerCase().includes(this.searchValue.toLowerCase())
    );
      
    }
  }

   // On change in sorting option.
  onSortChange = (field: string, order: string) => {
    this.userList = this._sharedService.sorting(
      this.userList,
      field,
      order
    );
  };

}
