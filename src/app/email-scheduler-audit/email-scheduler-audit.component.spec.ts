import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailSchedulerAuditComponent } from './email-scheduler-audit.component';

describe('EmailSchedulerAuditComponent', () => {
  let component: EmailSchedulerAuditComponent;
  let fixture: ComponentFixture<EmailSchedulerAuditComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EmailSchedulerAuditComponent]
    });
    fixture = TestBed.createComponent(EmailSchedulerAuditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
