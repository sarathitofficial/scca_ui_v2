// Default import
import { Component } from '@angular/core';

// Component related service import
import { ApiService } from '../services/api.service';

// Third party service import
import { ToastrService } from 'ngx-toastr';

// Shared service import
import { SharedService } from '../services/shared.service';
import { environment } from 'src/assets/environments/environment';

@Component({
  selector: 'app-email-scheduler-audit',
  templateUrl: './email-scheduler-audit.component.html',
  styleUrls: ['./email-scheduler-audit.component.scss'],
})
export class EmailSchedulerAuditComponent {
  // String type variables
  searchKey: string = '';
  startDateTime: string = '';
  endDateTime: string = '';

  // Boolean type variables
  showFilterOption: boolean = false;

  // List type variables
  emailSchedularAuditList: any[] = [];
  screenNameList: any[] = [];
  auditTypes: any[] = [];
  selectedTypes: string[] = [];
  selectedModules: string[] = [];

  constructor(
    private _sharedService: SharedService,
    private _toastr: ToastrService,
    private _apiService: ApiService
  ) {}

  ngOnInit = () => {
    this.clear();
    this.auditTypes = environment.auditTypes;
  };

  // Method to reset/clear all fields.
  clear = () => {
    this.emailSchedularAuditList = [];
    this.screenNameList = [];
    this.searchKey = '';
    this.auditTypes = [];
    this.startDateTime = '';
    this.endDateTime = '';
    this.selectedTypes = [];
    this.selectedModules = [];

    this.getAllEmailSchedularAuditData();
    this.getAllScreenDetails();
  };

  // Method to call when search icon is pressed.
  search = () => {
    // Logic which filter the emailSchedularAuditList with searchKey string value.
  };

  // Method to call when reload button is pressed
  reload = () => {
    this.clear();
  };

  // On change in sorting option.
  onSortChange = (field: string, order: string) => {
    this.emailSchedularAuditList = this._sharedService.sorting(
      this.emailSchedularAuditList,
      field,
      order
    );
  };

  // Method to Get All Datasource from Database.
  getAllEmailSchedularAuditData = () => {
    this._apiService
      .makeAPICall({
        action: 'getAllEmailSchedularAuditData',
        searchKey: this.searchKey,
      })
      .subscribe((response) => {
        if (response && response.status) {
          if (response.data.length > 0) {
            this.emailSchedularAuditList = response.data;
            this._toastr.success('Data Fetched Successfully');
          } else {
            this._toastr.warning('No Data Found');
          }
        }
      });
  };

  // Method to Get All Screen Name
  getAllScreenDetails = () => {
    this._sharedService
      .getAllScreenDetails({
        action: 'getAllScreenDetails',
      })
      .subscribe((response) => {
        if (response && response.status) {
          if (response.data.length > 0) {
            this.screenNameList = response.data;
            this._toastr.success('Data Fetched Successfully');
          } else {
            this._toastr.warning('No Data Found');
          }
        }
      });
  };

  // Export

  exportData = () => {};
}
