import { Component, ElementRef, OnInit, Renderer2, ViewChild, OnDestroy, NgZone, TemplateRef, ViewContainerRef } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ToastrService } from 'ngx-toastr';
import { MessageService } from 'primeng/api';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
import { HttpClient } from '@angular/common/http';
// Shared service import
import { SharedService, ReportBuilderTab } from 'src/app/services/shared.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { environment } from 'src/assets/environments/environment';
import { AppComponent } from '../app.component';
import { Directory, ReportBuilder, Field, Parameter } from '../model/common.model';




@Component({
  selector: 'app-report-builder',
  templateUrl: './report-builder.component.html',
  styleUrls: ['./report-builder.component.scss'],
})
export class ReportBuilderComponent {
  @ViewChild('staticTabs', { static: false }) staticTabs?: TabsetComponent;
  @ViewChild('tabConfirmationTemplate', { read: TemplateRef }) tabConfirmationTemplate!: TemplateRef<any>;

  addEditFolderModalRef!: BsModalRef;
  parameterModalRef!: BsModalRef;
  addEditReportBuilderModalRef!: BsModalRef;
  fieldModalRef!: BsModalRef;
  customFieldModalRef!: BsModalRef;
  tabConfirmationModalRef!: BsModalRef;

  tabList: ReportBuilderTab[] = [];
  directoryPath: any[] = [];

  // String type variables
  spinnerComponent = '';
  searchKey: string = '';
  currentStepperPage = '';
  selectedParameterIndex: any;
  selectedFieldIndex: any;
  isEdit: any;
  isFieldEdit: any;
  selectedDeletePopup = 'folder';

  showRootFolders = true;
  showErrors = false;
  isDirectoryFieldsValid = false;
  isBasicFieldsValid = false;
  isConfigFieldsValid = false;
  isParameterFieldsValid = false;
  isOutputFieldsValid = false;

  // Object Type variables
  directory: Directory;
  reportbuilder: any;
  parameter: any;
  field: any;
  reportType = [{ label: '', value: '' }];
  queryTypes = [{ label: '', value: '' }];
  reportbuilderFieldResult = [{ label: '', value: '' }];
  reportbuilderFieldDataTypes = [{ label: '', value: '' }];
  selectedDataSource: any = {};


  //Boolean values
  showTabPanel: boolean = false;
  currentStep = 1;
  numSteps = 4;
  configPage: boolean = false;
  basicDetailspage: boolean = false;
  spinnerTriggered: boolean = false;


  // List type variables
  collectionList: any[] = [];
  dataSourceList: any[] = [];
  parameterList: any[] = [];
  fieldList: any[] = [];
  masterData: any;


  constructor(
    private _toastr: ToastrService, private _apiService: ApiService,
    public sharedService: SharedService, private router: Router,
    public messageService: MessageService, private renderer: Renderer2, private elRef: ElementRef,
    private modalService: BsModalService,
    private appComponent: AppComponent
  ) {
    this.directory = new Directory();
  }


  nameAtoZ = "./assets/images/nameAtoZ.png";
  nameZtoA = "./assets/images/nameZtoA.png";
  DateOtoN = "./assets/images/DateOtoN.png";
  DateNtoO = "./assets/images/DateNtoO.png";
  categories: any[] = [
    {
      name: 'Name (A to Z)',
      key: 'A',
      img: "./assets/images/nameAtoZ.png"
    },
    {
      name: 'Name (Z to A)',
      key: 'Z',
      img: "./assets/images/nameZtoA.png"
    },
    {
      name: 'Date (Oldest to Newest)',
      key: 'O',
      img: "./assets/images/DateOtoN.png"
    },
    {
      name: 'Date (Newest to Oldest)',
      key: 'N',
      img: "./assets/images/DateNtoO.png"
    }
  ];

  ngOnInit() {
    this.appComponent.isLoggedIn = true;
    this.initializeStaticValues();
    this.pathSubscriber();
    if (!this.showRootFolders) {
      this.clear();
      this.loadDataList();
    }
  };

  //Triggered whenever the directory path is changed
  pathSubscriber() {
    this.sharedService.getDirectoryPath().subscribe(path => {
      this.clear();
      this.masterData={reportBuilderList:[],directoryList:[]};
      if (path?.length > 1) {
        this.showRootFolders = false;
        this.reportType[0] = {
          label: this.sharedService.getDirectoryPath().value[1].name,
          value: this.sharedService.getDirectoryPath().value[1].name
        };
        this.loadDataList();
      }
      else {
        this.showRootFolders = true;
        this.masterData.directoryList = environment.reportbuilderRootPath;
      }
    });
  }


  //Fields to be initialized once when the component is loaded
  initializeStaticValues() {
    //Dropdown values one time initialization
    this.queryTypes = environment.queryTypes;
    this.reportbuilderFieldResult = environment.reportbuilderFieldResult;
    this.reportbuilderFieldDataTypes = environment.reportbuilderFieldDataTypes;
    this.masterData = { reportBuilderList: [], directoryList: [] }
  }

  // Method to reset/clear all fields.
  clear() {
    this.dataSourceList = [];
    this.collectionList = [];
    this.searchKey = '';
    this.directory = new Directory();
    this.reportbuilder = new ReportBuilder();
  };

  loadDataList() {
    this.getScreenDirectories();
    this.getAllReportBuilder();
  }

  //Check data exist or not
  dataNotFound() {
    if (this.masterData?.reportBuilderList?.length > 0 || this.masterData?.directoryList?.length > 0) {
      return false;
    }
    return true
  }

  //Go back in main folder
  mainPageBackStep() {
    const selectedPath = this.sharedService.getDirectoryPath().value;
    selectedPath.pop();
    this.sharedService.setDirectoryPath(selectedPath);
  }

  // Method to call when search icon is pressed.
  search = () => {
    let searchkey = this.searchKey;
    this.clear();
    this.searchKey = searchkey;
    this.loadDataList();
  };

  // On change in sorting petion.
  onSortChange = (field: string, order: string) => {
    this.dataSourceList = this.sharedService.sorting(
      [],
      field,
      order
    );
  };


  //Retrieve all the report builders related to parent id and path from database
  getAllReportBuilder() {
    this.masterData.reportBuilderList=[];
    this._apiService
      .makeAPICall({
        action: 'getAllReportBuilder',
        searchkey: this.searchKey,
        parentid: this.sharedService.getDirectoryPath().value.slice(-1)[0].id,
        parentpath: (this.sharedService.getDirectoryPath().value.map(x => x.name).join('/')) + '/',
        loader: 'MainPageLoader'

      })
      .subscribe((response) => {
        if (response && response.status) {
          if (response?.data?.length > 0) {
            this.masterData.reportBuilderList = response.data;
          } else {
          }
          console.log('getAllReportBuilder response', response);
        }
      });
  }

  //Retrieve all the directories related to parent id and path from database
  getScreenDirectories() {
    this.masterData.directoryList=[];
    this._apiService
      .makeAPICall({
        action: 'getScreenDirectories',
        searchkey: this.searchKey,
        parentid: this.sharedService.getDirectoryPath().value.slice(-1)[0].id,
        rootdir: (this.sharedService.getDirectoryPath().value.map(x => x.name).join('/')) + '/',
        loader: 'MainPageLoader'

      })
      .subscribe((response) => {
        if (response && response.status) {
          if (response?.data?.length > 0) {
            this.masterData.directoryList = response.data;
          } else {
          }
          console.log('getscreendirectories response', response);
        }
      });
  }


  //Trigger when the next button is clicked in popup
  //Handle all the steppers with validation
  async nextStep() {

    console.log('reportbuilder data', this.reportbuilder);

    this.showErrors = true;

    switch (this.currentStepperPage) {
      case 'basic':
        await this.builderFieldsValidation('basic');
        if (!this.isBasicFieldsValid) {
          return;
        }
        else {
          this.spinnerComponent = 'basicnext';
          await this.getAllDataSourceIdName();
          this.showErrors = false;
          this.spinnerComponent = '';
        }
        break;
      case 'config':
        await this.builderFieldsValidation('config');

        if (!this.isConfigFieldsValid) {
          return;
        }
        else {
          this.spinnerComponent = 'confignext';
          if (!this.isEdit) {
            await this.getParameterFields();
          } else {
            await this.getParametersByReportBuilder();
            await this.getFieldsByReportBuilder();
          }
          await this.getAllCollectionIdName();
          this.spinnerComponent = '';
        }
        break;
      case 'params':
        // this.builderFieldsValidation('params');
        // if (!this.isParameterFieldsValid) {
        //   return;
        // }
        this.spinnerComponent='paramsnext';
        if (this.isEdit) {
          await this.getFieldsByReportBuilder();
        }
        this.spinnerComponent='';
        break;
      case 'fields':
        // this.builderFieldsValidation('fields');
        // if (!this.isOutputFieldsValid) {
        //   return;
        // }
        break;
      default:
        // Optional: Handle unexpected values of currentStepperPage
        console.log(`Unknown stepper page: ${this.currentStepperPage}`);
        break;
    }



    this.currentStep++;
    console.log("current step is:", this.currentStep);
    if (this.currentStep > this.numSteps) {
      this.currentStep = 1;
    }


    const steps = document.querySelectorAll('.step');

    steps.forEach((step: any, index: any) => {
      const stepNum = index + 1;
      if (stepNum === this.currentStep) {
        this.renderer.addClass(step, 'editing');
      } else {
        this.renderer.removeClass(step, 'editing');
      }
      if (stepNum < this.currentStep) {
        this.renderer.addClass(step, 'done');
      } else {
        this.renderer.removeClass(step, 'done');
      }
    });

    if (this.currentStep === 2) {
      this.currentStepperPage = 'config';
    } else if (this.currentStep === 3) {
      this.currentStepperPage = 'params';
    } else if (this.currentStep === 4) {
      this.currentStepperPage = 'fields';
    } else {
      this.currentStepperPage = 'basic';
    }
  }


  //Trigger when the back button is clicked in popup
  //validations also being done here
  backStep(step: any) {
    if (this.currentStep > 1) {
      this.currentStep--;
    } else {
      this.currentStep = this.numSteps;
    }
    const steps = document.querySelectorAll('.step');

    steps.forEach((step: any, index: any) => {
      const stepNum = index + 1;
      if (stepNum === this.currentStep) {
        this.renderer.addClass(step, 'editing');
      } else {
        this.renderer.removeClass(step, 'editing');
      }
      if (stepNum < this.currentStep) {
        this.renderer.addClass(step, 'done');
      } else {
        this.renderer.removeClass(step, 'done');
      }
    });
    if (this.currentStep === 2) {
      this.currentStepperPage = 'config';
    } else if (this.currentStep === 3) {
      this.currentStepperPage = 'params';
    } else if (this.currentStep === 4) {
      this.currentStepperPage = 'fields';
    } else {
      this.currentStepperPage = 'basic';
    }

  }




  //Directory or Folder Methods

  addEditFolderPopup(action: string, template: TemplateRef<any>) {

    if (action === 'open') {
      this.resetAddEditFolderPopup();
      this.addEditFolderModalRef = this.modalService.show(template, {
        backdrop: 'static',
        keyboard: false,
      });



      if (this.isEdit) {
        console.log('Edit directory', this.directory);

      }
      else {

        this.directory = new Directory();

        console.log('Add directory', this.directory);
      }
    } else {
      this.addEditFolderModalRef?.hide()
    }
  }

  //Reset folder fields
  resetAddEditFolderPopup() {
    this.showErrors = false;
    this.isDirectoryFieldsValid = false;
    this.spinnerComponent='';
  }

  directoryFieldValidation() {
    if (this.directory.name != '') {
      this.isDirectoryFieldsValid = true;
      return;
    }
  }

  //Method to form the object or model for Editing Folder/Directory data
  //Action - edit
  mapEditDirectory(data: any) {
    this.directory = new Directory;
    this.directory = {
      action: 'editDirectory',
      id: data.id,
      name: data.name,
      type: data.type,
      description: data.description,
      parentid: data.parentid,
      rootdir: data.rootdir,
      status: data.status,
      userid: 1
    }
  }

  // Add new or edit existing directory
  async storeDirectory() {

    this.spinnerComponent='storeDirectory';

    this.showErrors = true;
    this.directoryFieldValidation();

    if (!this.isDirectoryFieldsValid) {
      this._toastr.info('Please Fill the required fields');
      return;
    }

    if (this.isEdit) {

    }
    else {
      this.directory.parentid = this.sharedService.getDirectoryPath().value.slice(-1)[0].id;
      this.directory.rootdir = (this.sharedService.getDirectoryPath().value.map(x => x.name).join('/')) + '/';
    }

    console.log('store data', this.directory);

    try{
    //ADD - if directory fields are validated , Store it
    //EDIT -if directory fields are validated, save it
    let response=await this._apiService
      .makeAPICallAsync(this.directory);

        if (response.status && response.data.message.split('-')[1] === 'true') {
          this._toastr.success(('Folder ' + (this.isEdit ? 'Edited' : 'Added' + ' successfully')));
          this.clear();
          this.getScreenDirectories();
          this.addEditFolderModalRef?.hide();
          
        }
        else {
          this._toastr.error((this.isEdit ? 'Update Error' : 'Insert Error'), response.data.message.split('-')[0], { disableTimeOut: true });
        }
        console.log('storeReport response', response);
    }
    catch(error){
      console.log('storeReport error',error);
    }
    finally{
      this.spinnerComponent='';
    }
  }

  // Show or hide the delete popup commonly for both folder and files  
  toggleDeleteModal(action: string, template: TemplateRef<any>) {
    if (action === 'open')
      this.modalService.show(template, {
        backdrop: 'static',
        keyboard: false,
      });
    else if (action === 'close')
      this.modalService.hide();
  }

  // Method to Delete directory from Database.
  async deleteDirectory() {
    this.spinnerComponent = 'deleteDirectory';
    this.directory.action = 'deleteDirectory';

    try {

      let response = await this._apiService
        .makeAPICallAsync(this.directory);
      if (response.status && response.data.message.split('-')[1] === 'true') {
        this._toastr.success('Folder deleted successfully');
        this.clear();
        this.getScreenDirectories();
        this.modalService.hide();
      }
      else {
        this._toastr.error('Delete error', response.data.message.split('-')[0], { disableTimeOut: true });
      }
      console.log('deleteDirectory response', response);
    }
    catch (error) {
     console.log('deleteDirectory error',error)
    }
    finally {
      this.spinnerComponent = '';
    }
  };

  //Method to validation report builder all four stages
  async builderFieldsValidation(stepper: string) {
    switch (stepper) {
      case 'basic':
        if (this.reportbuilder.name !== '' && (this.reportbuilder.reporttype.value !== '') && ((this.reportbuilder.reporttype.value === 'Historical' || (this.reportbuilder.duration != null && this.reportbuilder.duration >= 10000)))) {
          this.isBasicFieldsValid = true;
        } else {
          this.isBasicFieldsValid = false;
        }
        break;
      case 'config':
        if (
          this.reportbuilder.datasourceid.id != 0 &&
          this.reportbuilder.querytype != '' &&
          this.reportbuilder.query != ''
        ) {
          this.isConfigFieldsValid = true;
        } else {
          this.isConfigFieldsValid = false;
        }
        break;
      default:
        // Handle unexpected stepper values if necessary
        console.log('Unexpected stepper value:', stepper);
        break;
    }
  }


  //Reset all the add edit report builder needed values 
  resetAddEditBuilderPopup() {
    this.selectedDataSource['connecteddatasource'] = '';
    this.parameterList = [];
    this.fieldList = [];
    this.isBasicFieldsValid = false;
    this.isConfigFieldsValid = false;
    this.isParameterFieldsValid = false;
    this.isOutputFieldsValid = false;
    this.showErrors = false;
    this.spinnerComponent='';
    this.currentStepperPage = 'basic';
    this.currentStep = 2;
    this.backStep(1);
  }

  //Show or hide Add/edit reportbuilder popup based on the action param
  //action - open - show popup
  //action - clse - close popup
  addEditReportBuilderPopup(action: string, template: TemplateRef<any>) {

    if (action === 'open') {
      this.resetAddEditBuilderPopup();
      this.addEditReportBuilderModalRef = this.modalService.show(template, {
        backdrop: 'static',
        keyboard: false,
      });

      if (this.isEdit) {

        // mapReportBuilderEditData
        this.currentStep = 1;
        const steps = document.querySelectorAll('.step');

        steps.forEach((step: any, index: any) => {
          if (index === 0) {
            this.renderer.addClass(step, 'editing');
            this.renderer.removeClass(step, 'done');
          } else {
            this.renderer.addClass(step, 'done');
            this.renderer.removeClass(step, 'editing');
          }
        });


        console.log('Edit Report builder', this.reportbuilder);

      }
      else {

        this.reportbuilder = new ReportBuilder;

        console.log('Add Report builder', this.reportbuilder);
      }
    }
    //Action- close- Hide the popup
    else {
      this.addEditReportBuilderModalRef?.hide()
    }
  }


  //Method to form the reportbuilder data for edit action
  mapReportBuilderEditData(rbdata: any) {
    this.reportbuilder = new ReportBuilder();
    this.reportbuilder = {
      action: 'editReportBuilder',
      id: rbdata.id,
      name: rbdata.name,
      description: rbdata.description,
      reporttype: this.reportType.find(x => x.value === rbdata.reporttype),
      duration: rbdata.duration,
      datasourceid: {name:'',id:rbdata.datasourceid},
      querytype: this.queryTypes.find(x => x.value === rbdata.querytype),
      query: rbdata.query,
      parameter: this.parameterList,
      field: this.fieldList,
      parentid: rbdata.parentid,
      parentpath: rbdata.parentpath,
      createdby: rbdata.createdby,
      createddate: rbdata.createddate,
      modifiedby: rbdata.modifiedby,
      modifieddate: rbdata.modifieddate,
    }
    console.log('mapReportBuilderEditData',this.reportbuilder);
  }

  //Method to store the added or edited reportbuilder in database
  storeReportBuilder() {
    this.spinnerComponent = 'submitAddEditReportBuilder';
    if (!this.isEdit) {

      this.reportbuilder.datasourceid = this.reportbuilder.datasourceid.id;
      this.reportbuilder.querytype = this.reportbuilder.querytype.value;
      this.reportbuilder.reporttype = this.reportbuilder.reporttype.value;
      this.reportbuilder.parentid = this.sharedService.getDirectoryPath().value.slice(-1)[0].id;
      this.reportbuilder.parentpath = (this.sharedService.getDirectoryPath().value.map(x => x.name).join('/')) + '/';

      for (let i = 0; i < this.parameterList?.length; i++) {
        this.parameterList[i].collectionid =this.parameterList[i].collectionid.id!=null
      ? this.parameterList[i].collectionid.id:0;
      }
      for (let i = 0; i < this.fieldList?.length; i++) {
        this.fieldList[i].datatype = this.fieldList[i].datatype.value;
        this.fieldList[i].result = this.fieldList[i].result.value;
      }
      this.reportbuilder.parameters = this.parameterList;
      this.reportbuilder.fields = this.fieldList;
    }

    try {
      this._apiService
        .makeAPICall(this.reportbuilder)
        .subscribe((response) => {

          if (response.status && response.data.message.split('-')[1] === 'true') {
            this._toastr.success(('Report Builder ' + (this.isEdit ? 'updated' : 'inserted' + ' successfully')));
            this.clear();
            this.getAllReportBuilder()
            this.addEditReportBuilderModalRef?.hide()
          }
          else {
            this._toastr.error((this.isEdit ? 'Update Error' : 'Insert Error'), response.data.message.split('-')[0], { disableTimeOut: true });
          }
          console.log('storeReportBuilder response', response);

        });
    }
    catch (error) {
      console.log('storeReportBuilder', error);
    }
    finally {
      this.spinnerComponent = '';
    }

    console.log('parameterlist', this.parameterList);
    console.log('fieldList', this.fieldList);
    console.log('report builder', this.reportbuilder);
  }


  // delete the existing reportbuilder from database
  async deleteReportBuilder() {
    this.spinnerComponent='deleteReportBuilder';
    console.log('report builder', this.reportbuilder);
    this.reportbuilder.action = 'deleteReportBuilder';
    try{
    let response = await this._apiService
      .makeAPICallAsync(this.reportbuilder);
    if (response.status && response.data.message.split('-')[1] === 'true') {
      this._toastr.success('Report builder deleted successfully');
      this.clear();
      this.getAllReportBuilder();
      this.modalService.hide();
    }
    else {
      this._toastr.error('Delete error', response.data.message.split('-')[0], { disableTimeOut: true });
    }
  }
  catch(error){
    console.log('deleteReportBuilder error',error);

  }
  finally{
    this.spinnerComponent='';
  }

  }

  // Method to Get All Datasource from Database.
  async getAllDataSourceIdName() {
    let response = await this._apiService
      .makeAPICallAsync({
        action: 'getAllDataSourceIdName',
        searchKey: this.searchKey,
      });
    if (response && response.status) {
      if (response.data?.length > 0) {
        this.dataSourceList = response.data;
        console.log("data source list id name", this.dataSourceList);
        if (this.isEdit && this.reportbuilder.datasourceid != undefined) {
          this.reportbuilder.datasourceid = this.dataSourceList.find(x => x.id === this.reportbuilder.datasourceid.id)
        }
      } else {
      }
    }
  };

  // Method to Get All Datasource from Database.
  async getAllCollectionIdName() {
    let response = await this._apiService
      .makeAPICallAsync({
        action: 'getAllCollectionIdName',
        searchKey: this.searchKey,
      });
    if (response && response.status) {
      if (response.data?.length > 0) {
        this.collectionList = response.data;
        console.log("collection list id name", this.collectionList);
      } else {
      }
    }
  };

  //Method to get datasource with his state (primary or secondary)
  selectDataSource(id: number, name: string) {
    this._apiService
      .makeAPICall({
        action: 'getDataSource',
        id: id
      })
      .subscribe((response: any) => {
        if (response && response.status) {
          if (response?.data?.length > 0) {
            let dsType = response.data[0].connecteddatasource === 'primary'
              || response.data[0].connecteddatasource === 'secondary'
              ? response.data[0].connecteddatasource.substring(0, 3) : '';

            if (dsType != '') {
              this.selectedDataSource['connecteddatasource'] = response.data[0].connecteddatasource;
              this.selectedDataSource['host'] = response.data[0][dsType + 'host'];
              this.selectedDataSource['port'] = response.data[0][dsType + 'port'];
              this.selectedDataSource['username'] = response.data[0][dsType + 'username'];
              this.selectedDataSource['password'] = response.data[0][dsType + 'password'];
              this.selectedDataSource['database'] = response.data[0][dsType + 'databasename'];
              this.reportbuilder.datasourceid = { name: name, id: id };
            }
            else {
              this.selectedDataSource['connecteddatasource'] = 'none';
            }
            console.log("selected data source", this.selectedDataSource);
          } else {
          }
        }
      });
  }


  //Method to get Parameter and fields based on the stored procedure
  async getParameterFields() {
    let response = await this._apiService
      .makeAPICallAsync({
        action: 'getParameterFields',
        datasource: this.selectedDataSource,
        storedprocedurename: this.reportbuilder.query
      });
    if (response && response.status) {

      // Insert parameters to parameter list
      for (let i = 0; i < response.data.parameters?.length; i++) {
        let param = new Parameter();
        param.name = response.data.parameters[i].name;
        param.alias = response.data.parameters[i].name;
        param.datatype = response.data.parameters[i].datatype;
        param.reportbuilderid=0;
        this.parameterList.push(param);
      }

      console.log('Parameter List', this.parameterList);

      // Insert parameters to parameter list
      for (let i = 0; i < response.data.fields?.length; i++) {
        let field = new Field();
        field.name = response.data.fields[i].name;
        field.alias = response.data.fields[i].name;
        field.datatype = {
          label: response.data.fields[i].datatype,
          value: response.data.fields[i].datatype
        };
        this.fieldList.push(field);
      }

      console.log('Field List', this.fieldList);


    } else {

    }
  }

  //Method to get Parameter by report builder
  async getParametersByReportBuilder() {
    let response = await this._apiService
      .makeAPICallAsync({
        action: 'getParametersByReportBuilder',
        reportbuilderid: this.reportbuilder.id
      });
    if (response && response.status) {

      // Insert parameters to parameter list
      for (let i = 0; i < response.data?.length; i++) {
        let param = new Parameter();
        param.id = response.data[i].id;
        param.reportbuilderid = response.data[i].reportbuilderid;
        param.name = response.data[i].name;
        param.alias = response.data[i].name;
        param.datatype = response.data[i].datatype;
        param.value = response.data[i].value;
        param.allownull = response.data[i].allownull;
        param.collectionid = response.data[i] = { name: '', id: response.data[i].collectionid };

        this.parameterList.push(param);
      }


      console.log('getParametersByReportBuilder', this.parameterList);

    } else {
    }
    console.log('getParametersByReportBuilder reponse', response);
  }

  //Method to get Fields by report builder
  async getFieldsByReportBuilder() {

    let response = await this._apiService
      .makeAPICallAsync({
        action: 'getFieldsByReportBuilder',
        reportbuilderid: this.reportbuilder.id
      });


    console.log('getFieldsByReportBuilder reponse', response);

    if (response && response.status) {


      // Insert fields to field list
      for (let i = 0; i < response.data?.length; i++) {
        let field = new Field();
        field.id = response.data[i].id;
        field.name = response.data[i].name;
        field.alias = response.data[i].alias;
        field.datatype = {
          label: response.data[i].datatype,
          value: response.data[i].datatype
        };

        this.fieldList.push(field);
      }

      console.log('getFieldsByReportBuilder', this.fieldList);


    } else {

    }


  }

  //Show or hide the parameter popup
  parameterPopup(action: string, template: TemplateRef<any>) {
    if (action === 'open') {
      this.parameterModalRef = this.modalService.show(template, {
        backdrop: 'static',
        keyboard: false
      });
      this.parameter = new Parameter();
      this.parameter = Object.assign({}, this.parameterList[this.selectedParameterIndex]);


    } else {
      this.parameterModalRef.hide();
    }
  }

  //save the edited parameter to parameter list 
  storeParameter() {
    this.parameterList[this.selectedParameterIndex] = Object.assign({}, this.parameter);
    this.parameterModalRef.hide();
  }


  //return collection name based on its id
  getParameterCollectionName(id: number) {
    return this.collectionList.find(x => x.id === id)?.name;
  }

  //Handle showing or hiding the field popup
  fieldPopup(action: string, template: TemplateRef<any>) {
    if (action === 'open') {
      this.fieldModalRef = this.modalService.show(template, {
        backdrop: 'static',
        keyboard: false
      });
      this.field = new Field();
      this.field = Object.assign({}, this.fieldList[this.selectedFieldIndex]);

    } else {
      this.fieldModalRef.hide();
    }
  }

  //Set the edited or added field data to field list
  storeField() {
    this.fieldList[this.selectedFieldIndex] = Object.assign({}, this.field);
    this.fieldModalRef.hide();
  }

  //Triggered when the field result dropdown is selected
  onChangingFieldResult(option: any) {
    if (option != null || option != undefined) {
      this.field.result = option;
      if (option?.value === 'custom') {
        this.field.iscustomresult = true;
      }
      else {
        this.field.iscustomresult = false;
      }
    }
  }

  //Handle showing or hiding the add/edit custom field popup
  customFieldPopup(action: string, template: TemplateRef<any>, type: string) {
    if (action === 'open') {
      this.customFieldModalRef = this.modalService.show(template, {
        backdrop: 'static',
        keyboard: false
      });
      this.field = new Field();
      this.field.iscustomfield = true;
      if (type != 'add') {
        this.field = Object.assign({}, this.fieldList[this.selectedFieldIndex]);
      }
    } else {
      this.customFieldModalRef.hide();
    }
  }

  //Store the added or edited custom field in list
  storeCustomField(action: string) {
    if (action === 'add') {
      this.fieldList.push(Object.assign({}, this.field));
    }
    else {
      this.fieldList[this.selectedFieldIndex] = Object.assign({}, this.field);
    }
    this.customFieldModalRef?.hide();
  }

  //Handle the folder drill down
  onFolderClicked(folder: any) {
    const path = this.sharedService.getDirectoryPath().value;
    path.push({ name: folder.name, id: folder.id });
    this.sharedService.setDirectoryPath(path);
  }


  // Method to handle adding new tab or open existing tab when the builder is clicked
  onReportBuilderClicked(rbdata: any, checkExistTab: boolean, showModal: boolean) {

    if (showModal)
      this.mapReportBuilderEditData(rbdata);

    const newTab = {
      title: this.reportbuilder.name,
      content: this.reportbuilder.description,
      disabled: false,
      removable: true,
      data: this.reportbuilder,
      id: this.reportbuilder.id,
      type: 'file'
    };

    let index: any;
    if (checkExistTab && this.tabList.some(x => x.id == this.reportbuilder.id && x.type === 'file')) {
      if (showModal) {
        this.tabConfirmationModalRef = this.modalService.show(this.tabConfirmationTemplate, {
          backdrop: 'static',
          keyboard: false
        });
        return;
      }
      else {
        this.tabConfirmationModalRef?.hide();
      }

      index = this.tabList.findIndex(x => x.id == this.reportbuilder.id) + 1;
    }
    else {
      this.tabList.push(newTab);
      index = this.tabList?.length;
      this.customFieldModalRef?.hide();
    }

    this.showTabPanel = true;
    setTimeout(() => {
      this.selectTab(index)
    }, 0)
  }


  //Select the active tab
  selectTab(tabId: number) {
    if (this.staticTabs?.tabs[tabId]) {
      this.staticTabs.tabs[tabId].active = true;
    }
  }

  //Remove tab from tab list
  removeTab(tab: ReportBuilderTab): void {
    this.tabList.splice(this.tabList.indexOf(tab), 1);
    console.log('Remove Tab handler');
  }

}

