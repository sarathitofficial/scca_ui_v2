import { Component, ElementRef, OnInit, Renderer2, ViewChild, OnDestroy, NgZone, TemplateRef } from '@angular/core';
import { ChangeDetectorRef } from '@angular/core';
import { MessageService } from 'primeng/api';
import { MenuItem } from 'primeng/api';
import { StepsModule } from 'primeng/steps';
import { Subscription } from 'rxjs';
// Shared service import
import { Router } from '@angular/router';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
// Component related service import
import { ApiService } from '../services/api.service';

// Third party service import
import { ToastrService } from 'ngx-toastr';

// Shared service import
import { CollectionTab, SharedService } from '../services/shared.service';
import { Collection, CollectionMaster, Directory } from '../model/common.model';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { AppComponent } from '../app.component';
import { environment } from 'src/assets/environments/environment';
import { TimeScale } from 'chart.js';
interface country {
  name?: string;
  code?: string;
}

interface ITab {
  title: string;
  content: string;
  removable: boolean;
  disabled: boolean;
  active?: boolean;
  customClass?: string;
}


@Component({
  selector: 'app-collection',
  templateUrl: './collection.component.html',
  styleUrls: ['./collection.component.scss'],
})
export class CollectionComponent {

  //New begin

  @ViewChild('staticTabs', { static: false }) staticTabs?: TabsetComponent;
  @ViewChild('tabConfirmationTemplate', { read: TemplateRef }) tabConfirmationTemplate!: TemplateRef<any>;

  addEditFolderModalRef!: BsModalRef;
  addEditCollectionModalRef!: BsModalRef;
  collectionValuesModalRef!: BsModalRef;
  tabConfirmationModalRef!: BsModalRef;

  tabList: CollectionTab[] = [];
  directoryPath: any[] = [];

  // String type variables
  spinnerComponent = '';
  searchKey: string = '';
  currentStepperPage = '';
  isEdit: any;
  selectedDeletePopup = 'folder';

  showErrors = false;
  isDirectoryFieldsValid = false;
  isBasicFieldsValid = false;
  isConfigFieldsValid = false;
  showRootFolders = true;

  // Object Type variables
  directory: Directory;
  collection: any;
  selectedDataSource: any = {};

  //Boolean values
  showTabPanel: boolean = false;
  currentStep = 1;
  numSteps = 4;


  // List type variables
  dataSourceList: any[] = [];
  masterData: CollectionMaster = new CollectionMaster();


  //New Ends



  // String type variables
  tabsOverviewShow: boolean = false;
  formattedDate: any;
  dateString: string = "2024-03-10T 10:10:10"; // Note: Use 'T' instead of space for ISO 8601 compliance
  dateObject = new Date(this.dateString);
  // List type variables
  activeIndex: number | undefined = 0;
  index = 0;
  active = 0;

  nameAtoZ = "./assets/images/nameAtoZ.png";
  nameZtoA = "./assets/images/nameZtoA.png";
  DateOtoN = "./assets/images/DateOtoN.png";
  DateNtoO = "./assets/images/DateNtoO.png";
  categories: any[] = [
    {
      name: 'Name (A to Z)',
      key: 'A',
      img: "./assets/images/nameAtoZ.png"
    },
    {
      name: 'Name (Z to A)',
      key: 'Z',
      img: "./assets/images/nameZtoA.png"
    },
    {
      name: 'Date (Oldest to Newest)',
      key: 'O',
      img: "./assets/images/DateOtoN.png"
    },
    {
      name: 'Date (Newest to Oldest)',
      key: 'N',
      img: "./assets/images/DateNtoO.png"
    }
  ];

  constructor(
    private _toastr: ToastrService, private _apiService: ApiService,
    public sharedService: SharedService, private router: Router,
    public messageService: MessageService, private renderer: Renderer2, private elRef: ElementRef,
    private modalService: BsModalService,
    private appComponent: AppComponent
  ) {
    this.directory = new Directory();
    this.masterData = new CollectionMaster();
  }

  ngOnInit() {
    this.appComponent.isLoggedIn = true;
    this.initializeStaticValues();
    this.pathSubscriber();
  }

  //Triggered whenever the directory path is changed
  pathSubscriber() {
    this.sharedService.getDirectoryPath().subscribe(path => {
      this.clear();
      this.masterData = new CollectionMaster();

      if (path.length > 0) {
        this.loadDataList();
      }
      if (path.length == 1) {
        this.showRootFolders = false;
      }
      else {
        this.showRootFolders = true;
      }
    });
  }


  //Fields to be initialized once when the component is loaded
  initializeStaticValues() {
    this.masterData = new CollectionMaster();
  }

  // Method to reset/clear all fields.
  clear() {
    this.dataSourceList = [];
    this.searchKey = '';
    this.directory = new Directory();
    this.collection = new Collection();
  };

  loadDataList() {
    this.getScreenDirectories();
    this.getAllCollection();
  }


  //Check data exist or not
  dataNotFound() {
    if (this.masterData?.collectionList?.length > 0 || this.masterData?.directoryList?.length > 0) {
      return false;
    }
    return true
  }

  //Go back in main folder
  mainPageBackStep() {
    const selectedPath = this.sharedService.getDirectoryPath().value;
    selectedPath.pop();
    this.sharedService.setDirectoryPath(selectedPath);
  }

  // Method to call when search icon is pressed.
  search = () => {
    let searchkey = this.searchKey;
    this.clear();
    this.searchKey = searchkey;
    this.loadDataList();
  };

  // On change in sorting petion.
  onSortChange = (field: string, order: string) => {
    this.dataSourceList = this.sharedService.sorting(
      [],
      field,
      order
    );
  };

  //Method to get datasource with his state (primary or secondary)
  selectDataSource(id: number, name: string) {
    this._apiService
      .makeAPICall({
        action: 'getDataSource',
        id: id
      })
      .subscribe((response: any) => {
        if (response && response.status) {
          if (response?.data?.length > 0) {
            let dsType = response.data[0].connecteddatasource === 'primary'
              || response.data[0].connecteddatasource === 'secondary'
              ? response.data[0].connecteddatasource.substring(0, 3) : '';

            if (dsType != '') {
              this.selectedDataSource['connecteddatasource'] = response.data[0].connecteddatasource;
              this.selectedDataSource['host'] = response.data[0][dsType + 'host'];
              this.selectedDataSource['port'] = response.data[0][dsType + 'port'];
              this.selectedDataSource['username'] = response.data[0][dsType + 'username'];
              this.selectedDataSource['password'] = response.data[0][dsType + 'password'];
              this.selectedDataSource['database'] = response.data[0][dsType + 'databasename'];
              this.collection.datasourceid = { name: name, id: id };
            }
            else {
              this.selectedDataSource['connecteddatasource'] = 'none';
            }
            console.log("selected data source", this.selectedDataSource);
          } else {
          }
        }
      });
  }


  //Retrieve all the collection related to parent id and path from database
  getAllCollection() {
    this.masterData.collectionList = [];
    try {
      this._apiService
        .makeAPICall({
          action: 'getAllCollection',
          searchkey: this.searchKey,
          parentid: this.sharedService.getDirectoryPath().value.slice(-1)[0].id,
          parentpath: (this.sharedService.getDirectoryPath().value.map(x => x.name).join('/')) + '/',
          loader: 'MainPageLoader'
        })
        .subscribe((response) => {
          if (response && response.status) {
            if (response?.data?.length > 0) {
              this.masterData.collectionList = response.data;
            } else {
            }
            console.log('getAllCollection response', response);
          }
        });
    }
    catch (error) {
      console.log('getAllCollection error', error);
    }
    finally {

    }
  }

  //Retrieve all the directories related to parent id and path from database
  getScreenDirectories() {
    this.masterData.directoryList = [];
    this._apiService
      .makeAPICall({
        action: 'getScreenDirectories',
        searchkey: this.searchKey,
        parentid: this.sharedService.getDirectoryPath().value.slice(-1)[0].id,
        rootdir: (this.sharedService.getDirectoryPath().value.map(x => x.name).join('/')) + '/',
        loader: 'MainPageLoader'

      })
      .subscribe((response) => {
        if (response && response.status) {
          if (response?.data?.length > 0) {
            this.masterData.directoryList = response.data;
          } else {
          }
          console.log('getscreendirectories response', response);
        }
      });
  }


  //Trigger when the next button is clicked in popup
  //Handle all the steppers with validation
  async nextStep() {

    console.log('collection data', this.collection);

    this.showErrors = true;

    switch (this.currentStepperPage) {
      case 'basic':
        await this.collectionFieldsValidation('basic');
        if (!this.isBasicFieldsValid) {
          return;
        }
        else {
          this.spinnerComponent = 'basicnext';
          await this.getAllDataSourceIdName();
          if(this.isEdit){  
            await this.validateCollectionQuery(); 
          }
          this.showErrors = false;
          this.spinnerComponent = '';

        }
        break;

      default:
        // Optional: Handle unexpected values of currentStepperPage
        console.log(`Unknown stepper page: ${this.currentStepperPage}`);
        break;
    }

    this.currentStep++;
    console.log("current step is:", this.currentStep);
    if (this.currentStep > this.numSteps) {
      this.currentStep = 1;
    }

    const steps = document.querySelectorAll('.step');
    steps.forEach((step: any, index: any) => {
      const stepNum = index + 1;
      if (stepNum === this.currentStep) {
        this.renderer.addClass(step, 'editing');
      } else {
        this.renderer.removeClass(step, 'editing');
      }
      if (stepNum < this.currentStep) {
        this.renderer.addClass(step, 'done');
      } else {
        this.renderer.removeClass(step, 'done');
      }
    });

    if (this.currentStep === 2) {
      this.currentStepperPage = 'config';
    } else {
      this.currentStepperPage = 'basic';
    }
  }


  //Trigger when the back button is clicked in popup
  //validations also being done here
  backStep(step: any) {
    if (this.currentStep > 1) {
      this.currentStep--;
    } else {
      this.currentStep = this.numSteps;
    }
    const steps = document.querySelectorAll('.step');

    steps.forEach((step: any, index: any) => {
      const stepNum = index + 1;
      if (stepNum === this.currentStep) {
        this.renderer.addClass(step, 'editing');
      } else {
        this.renderer.removeClass(step, 'editing');
      }
      if (stepNum < this.currentStep) {
        this.renderer.addClass(step, 'done');
      } else {
        this.renderer.removeClass(step, 'done');
      }
    });
    if (this.currentStep === 2) {
      this.currentStepperPage = 'config';
    } else if (this.currentStep === 3) {
      this.currentStepperPage = 'params';
    } else if (this.currentStep === 4) {
      this.currentStepperPage = 'fields';
    } else {
      this.currentStepperPage = 'basic';
    }

  }


  //Directory or Folder Methods
  addEditFolderPopup(action: string, template: TemplateRef<any>) {

    if (action === 'open') {
      this.resetAddEditFolderPopup();
      this.addEditFolderModalRef = this.modalService.show(template, {
        backdrop: 'static',
        keyboard: false,
      });



      if (this.isEdit) {
        console.log('Edit directory', this.directory);

      }
      else {

        this.directory = new Directory();

        console.log('Add directory', this.directory);
      }
    } else {
      this.addEditFolderModalRef?.hide()
    }
  }

  //Reset folder fields
  resetAddEditFolderPopup() {
    this.spinnerComponent = '';
    this.showErrors = false;
    this.isDirectoryFieldsValid = false;
  }

  directoryFieldValidation() {
    if (this.directory.name != '') {
      this.isDirectoryFieldsValid = true;
      return;
    }
  }

  //Method to form the object or model for Editing Folder/Directory data
  //Action - edit
  mapEditDirectory(data: any) {
    this.directory = new Directory;
    this.directory = {
      action: 'editDirectory',
      id: data.id,
      name: data.name,
      type: data.type,
      description: data.description,
      parentid: data.parentid,
      rootdir: data.rootdir,
      status: data.status,
      userid: 1
    }
  }

  // Add new or edit existing directory
  async storeDirectory() {

    this.spinnerComponent = 'storeDirectory';

    this.showErrors = true;
    this.directoryFieldValidation();

    if (!this.isDirectoryFieldsValid) {
      this._toastr.info('Please Fill the required fields');
      return;
    }

    if (this.isEdit) {

    }
    else {
      this.directory.parentid = this.sharedService.getDirectoryPath().value.slice(-1)[0].id;
      this.directory.rootdir = (this.sharedService.getDirectoryPath().value.map(x => x.name).join('/')) + '/';
    }

    console.log('store data', this.directory);

    try {
      //ADD - if directory fields are validated , Store it
      //EDIT -if directory fields are validated, save it
      let response = await this._apiService
        .makeAPICallAsync(this.directory);

      if (response.status && response.data.message.split('-')[1] === 'true') {
        this._toastr.success(('Folder ' + (this.isEdit ? 'Edited' : 'Added' + ' successfully')));
        this.clear();
        this.getScreenDirectories();
        this.addEditFolderModalRef?.hide();

      }
      else {
        this._toastr.error((this.isEdit ? 'Update Error' : 'Insert Error'), response.data.message.split('-')[0], { disableTimeOut: true });
      }
      console.log('storeDirectory response', response);
    }
    catch (error) {
      console.log('storeDirectory error', error);
    }
    finally {
      this.spinnerComponent = '';
    }
  }

  // Show or hide the delete popup commonly for both folder and files  
  toggleDeleteModal(action: string, template: TemplateRef<any>) {
    if (action === 'open')
      this.modalService.show(template, {
        backdrop: 'static',
        keyboard: false,
      });
    else if (action === 'close')
      this.modalService.hide();
  }

  // Method to Delete directory from Database.
  async deleteDirectory() {
    this.spinnerComponent = 'deleteDirectory';
    this.directory.action = 'deleteDirectory';

    try {

      let response = await this._apiService
        .makeAPICallAsync(this.directory);
      if (response.status && response.data.message.split('-')[1] === 'true') {
        this._toastr.success('Folder deleted successfully');
        this.clear();
        this.getScreenDirectories();
        this.modalService.hide();
      }
      else {
        this._toastr.error('Delete error', response.data.message.split('-')[0], { disableTimeOut: true });
      }
      console.log('deleteDirectory response', response);
    }
    catch (error) {
      console.log('deleteDirectory error', error)
    }
    finally {
      this.spinnerComponent = '';
    }
  };

  //Method to validate collection two stages stepper fields
  async collectionFieldsValidation(stepper: string) {
    switch (stepper) {
      case 'basic':
        if (this.collection.name != '') {
          this.isBasicFieldsValid = true;
        } else {
          this.isBasicFieldsValid = false;
        }
        break;

      case 'config':
        if (
          this.collection.datasourceid.id != 0 &&
          this.collection.query != ''
        ) {
          this.isConfigFieldsValid = true;
        } else {
          this.isConfigFieldsValid = false;
        }
        break;

      default:
        // Handle unexpected stepper values if necessary
        console.log('Unexpected stepper value:', stepper);
        break;
    }
  }


  //Reset all the add edit collection needed values 
  resetAddEditCollectionPopup() {
    this.spinnerComponent = '';
    this.selectedDataSource['connecteddatasource'] = '';
    this.isBasicFieldsValid = false;
    this.isConfigFieldsValid = false;
    this.showErrors = false;
    this.collection.queryvalidation = '';
    this.currentStepperPage = 'basic';
    this.currentStep = 2;
    this.backStep(1);
  }

  //Show or hide Add/edit collection popup based on the action param
  //action - open - show popup
  //action - clse - close popup
  addEditCollectionPopup(action: string, template: TemplateRef<any>) {

    if (action === 'open') {
      this.resetAddEditCollectionPopup();
      this.addEditCollectionModalRef = this.modalService.show(template, {
        backdrop: 'static',
        keyboard: false,
      });

      if (this.isEdit) {

        this.currentStep = 1;
        const steps = document.querySelectorAll('.step');

        steps.forEach((step: any, index: any) => {
          if (index === 0) {
            this.renderer.addClass(step, 'editing');
            this.renderer.removeClass(step, 'done');
          } else {
            this.renderer.addClass(step, 'done');
            this.renderer.removeClass(step, 'editing');
          }
        });


        this.collection.queryvalidation = 'success';
        console.log('editCollection', this.collection);

      }
      else {

        this.collection = new Collection();
        console.log('addCollection', this.collection);
      }
    }
    //Action- close- Hide the popup
    else {
      this.addEditCollectionModalRef?.hide()
    }
  }

  //Method to form the collection data for edit action
  mapCollectionEditData(collectionRow: any) {
    this.collection = new Collection();
    this.collection = {
      action: 'editCollection',
      id: collectionRow.id,
      name: collectionRow.name,
      description: collectionRow.description,
      datasourceid: { name: '', id: collectionRow.datasourceid },
      query: collectionRow.query,
      collectionvalues: [],
      parentid: collectionRow.parentid,
      status: 'active',
      queryvalidation: '',
      parentpath: collectionRow.parentpath,
      createdby: collectionRow.createdby,
      createddate: collectionRow.createddate,
      modifiedby: collectionRow.modifiedby,
      modifieddate: collectionRow.modifieddate,

    }
    console.log('mapCollectionEditData', this.collection);
  }



  //Method to store the added or edited collection in database
  async storeCollection() {

    this.spinnerComponent = 'submitAddEditCollection';
      if (this.isEdit) {
        this.collectionFieldsValidation('basic');
        if (!this.isBasicFieldsValid) {
          this._toastr.warning('Please fill the required basic fields');
          this.spinnerComponent = '';
          return;
        }
      }

      // Query format validation
      if (this.collection.queryvalidation != "success") {
        this._toastr.warning('Please validate the query. If already done, try again!');
        this.spinnerComponent = '';
        return;
      }

      // Query and datasource value required validation
      this.collectionFieldsValidation('config');
      if (!this.isConfigFieldsValid) {
        this._toastr.warning('Please fill the required query fields');
        this.spinnerComponent = '';
        return;
      }

      //code cleaning
      this.collection.datasourceid = this.collection.datasourceid.id;
      this.collection.queryvalidation = '';
      delete this.collection.collectionvalues;

      if (!this.isEdit) {
        this.collection.parentid = this.sharedService.getDirectoryPath().value.slice(-1)[0].id;
        this.collection.parentpath = (this.sharedService.getDirectoryPath().value.map(x => x.name).join('/')) + '/';
      }

      
    try {
      let response = await this._apiService
        .makeAPICallAsync(this.collection);

      if (response.status && response.data.message.split('-')[1] === 'true') {
        this._toastr.success(('Collection ' + (this.isEdit ? 'updated' : 'inserted' + ' successfully')));
        this.clear();
        this.getAllCollection()
        this.addEditCollectionModalRef?.hide()
      }
      else {
        this._toastr.error((this.isEdit ? 'Update Error' : 'Insert Error'), response.data.message.split('-')[0], { disableTimeOut: true });
      }
      console.log('storeCollection response', response);

    }
    catch (error) {
      console.log('storeCollection', error);
    }
    finally {
      this.spinnerComponent = '';
      this.collection.queryvalidation = '';
    }
    console.log('collection data', this.collection);
  }

  // delete the existing collection from database
  async deleteCollection() {
    this.spinnerComponent = 'deleteCollection';
    console.log('collection', this.collection);
    try {
      this.collection.action = 'deleteCollection';
      this.collection.datasourceid = this.collection.datasourceid.id
      delete this.collection.collectionvalues;
      let response = await this._apiService
        .makeAPICallAsync(this.collection);
      if (response.status && response.data.message.split('-')[1] === 'true') {
        this._toastr.success('Collection deleted successfully');
        this.clear();
        this.getAllCollection();
        this.modalService.hide();
      }
      else {
        this._toastr.error('Delete error', response.data.message.split('-')[0], { disableTimeOut: true });
      }
    }
    catch (error) {
      console.log('deleteCollection error', error);
    }
    finally {
      this.spinnerComponent = '';
    }

  }

  // Method to Get All Datasource from Database.
  async getAllDataSourceIdName() {
    try {
      let response = await this._apiService
        .makeAPICallAsync({
          action: 'getAllDataSourceIdName',
          searchKey: this.searchKey,
        });
      if (response && response.status) {
        if (response.data?.length > 0) {
          console.log("getAllDataSourceIdName response", response);
          this.dataSourceList = response.data;
          if (this.isEdit && this.collection.datasourceid != undefined) {
            this.collection.datasourceid = this.dataSourceList.find(x => x.id === this.collection.datasourceid.id)
          }
        } else {
        }
      }
    }
    catch (error) {
      console.log("getAllDataSourceIdName error", error);
    }
    finally {
    }
  }

  //Validate and retrieve the collection values based on the query
  async validateCollectionQuery() {
    this.collection.collectionvalues = [];
    this.collection.queryvalidation = '';
    this.spinnerComponent = 'validateQuery';

    try {
      let response = await this._apiService.makeAPICallAsync({
        action: 'validateCollectionQuery',
        id: this.collection.datasourceid.id,
        query: this.collection.query
      });

      if (response && response.status) {
        let fields = response.data.fields;
        console.log('validateCollectionQuery response', response.data);
        if (fields.length == 2 && fields.some((x: { name: string; }) => x.name === 'id') && fields.some((x: { name: string; }) => x.name === 'key')) {
          this.collection.collectionvalues = response.data.rows;
          this.collection.queryvalidation = "success";
          this._toastr.success('Query Validation success', 'Success');
        }
        else {
          this.collection.queryvalidation = "failure";
          this._toastr.error('Query should be in correct format. Please check the format and validate again', 'Error', { disableTimeOut: true });
        }
      } else {
        this.collection.queryvalidation = "failure";
        this._toastr.error('Error in retrieving and validating collection query', 'Error');
      }
      console.log('validateCollectionQuery response', response);
    }
    catch (error) {
      this._toastr.error('Error in retrieving and validating collection query', 'Error');
      console.log('validateCollectionQuery error', error);
    }
    finally {
      this.spinnerComponent = '';
    }
  }


  //Handle the folder drill down
  onFolderClicked(folder: any) {
    const path = this.sharedService.getDirectoryPath().value;
    path.push({ name: folder.name, id: folder.id });
    this.sharedService.setDirectoryPath(path);
  }

  collectionRecordsPopup(action: string, template: TemplateRef<any>) {

    if (action === 'open') {
      this.collectionValuesModalRef = this.modalService.show(template, {
        backdrop: 'static',
        keyboard: false,
      });
    } else {
      this.collectionValuesModalRef?.hide();
    }
  }


  // Method to handle adding new tab or open existing tab when the collection file is clicked
  onCollectionFileClicked(collection: any, checkExistTab: boolean, showModal: boolean) {

    if (showModal)
      this.mapCollectionEditData(collection);

    const newTab = {
      title: this.collection.name,
      content: this.collection.description,
      disabled: false,
      removable: true,
      data: this.collection,
      id: this.collection.id,
      type: 'file'
    };

    let index: any;
    if (checkExistTab && this.tabList.some(x => x.id == this.collection.id && x.type === 'file')) {
      if (showModal) {
        this.tabConfirmationModalRef = this.modalService.show(this.tabConfirmationTemplate, {
          backdrop: 'static',
          keyboard: false
        });
        return;
      }
      else {
        this.tabConfirmationModalRef?.hide();
      }
      index = this.tabList.findIndex(x => x.id == this.collection.id) + 1;
    }
    else {
      this.tabList.push(newTab);
      index = this.tabList?.length;
      this.tabConfirmationModalRef?.hide();
    }

    this.showTabPanel = true;
    setTimeout(() => {
      this.selectTab(index)
    }, 0)
  }


  //Select the active tab
  selectTab(tabId: number) {
    if (this.staticTabs?.tabs[tabId]) {
      this.staticTabs.tabs[tabId].active = true;
    }
  }

  //Remove tab from tab list
  removeTab(tab: CollectionTab): void {
    this.tabList.splice(this.tabList.indexOf(tab), 1);
    console.log('Remove Tab handler');
  }












}
