import { Component, OnInit } from '@angular/core';
import { SharedService } from '../services/shared.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-all-services',
  templateUrl: './all-services.component.html',
  styleUrls: ['./all-services.component.scss']
})
export class AllServicesComponent implements OnInit {

  constructor(private sharedService:SharedService,private router:Router){

  }

  ngOnInit(): void {
  
  }

  routerLink(routePath: string,title:string,image:any): void {
    this.sharedService.setTitleDetails({name:title,imagesource:'./assets/images/'+image});
    this.sharedService.clearDirectoryPath();
    this.sharedService.setDirectoryPath([{name:title,id:0}]);
    this.router.navigateByUrl('/'+routePath);
  }

}
