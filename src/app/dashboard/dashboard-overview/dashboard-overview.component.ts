import { Component,ElementRef,OnInit, ViewChild  } from '@angular/core';
import { ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';

interface ITab {
  title: string;
  content: string;
  removable: boolean;
  disabled: boolean;
  active?: boolean;
  customClass?: string;
}
declare let $: any; 
@Component({
  selector: 'app-dashboard-overview',
  templateUrl: './dashboard-overview.component.html',
  styleUrls: ['./dashboard-overview.component.scss']
})
export class DashboardOverviewComponent {
  @ViewChild('open',{read: ElementRef})closeRef!:ElementRef<any>;
  @ViewChild('close',{read: ElementRef})modelRef!:ElementRef<any>;
 
constructor(private cdr: ChangeDetectorRef, private router:Router){}

activeIndex: number | undefined = 0;

foldername = [{
  name: 'Agent Statistics',
},{
  name:'June Test'
},{
  name:'Sample1'
},{
  name:'Sample2'
}];


tabs: ITab[] = [
  {
    title: 'April Month',
    content: 'Dynamic content 1',
    removable: false,
    disabled: false,
  },
  {
    title: 'May Month',
    content: 'Dynamic content 2',
    removable: false,
    disabled: false,
  },
  {
    title: 'June Month',
    content: 'Dynamic content 3',
    removable: true,
    disabled: false,
  },
];




activeIndexChange(index : any){
    this.activeIndex = index
}

ngAfterViewInit() {
  this.cdr.detectChanges();
}
 ngOnInit(): void {
     

 }

 dashboardview(foldername:any){
  this.router.navigate(['/Dashboard','/FolderOverview',foldername])
 }

 openModal(){
  this.modelRef.nativeElement.click()
 }
 closeModal(){
  this.closeRef.nativeElement.click()
 }

  addNewTab(): void {
    const newTabIndex = this.tabs.length + 1;
    this.tabs.push({
      title: `${newTabIndex}`,
      content: `${newTabIndex}`,
      disabled: false,
      removable: true,
    });
    this.closeModal();
  }

  removeTabHandler(tab: ITab): void {
    this.tabs.splice(this.tabs.indexOf(tab), 1);
    console.log('Remove Tab handler');
  }





}
