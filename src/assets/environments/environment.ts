export const environment = {
  apiUrl: 'https://7finmcpazj.execute-api.us-east-1.amazonaws.com/dev/sv_dev_sccaapi_v1',
  auditTypes: ['Insert', 'Delte', 'Update'],
  sqlLanguageTypes :[{label:'PG Sql',value:'pgsql'}],
  cognito: {
    userPoolId: 'us-east-1_eqXEco5DX', 
    userPoolWebClientId: '49sniu2t8ci82etudn5atodg5n' 
  },
  backgrounderLoader:['testDBConnection','getAllDataSource'],
  queryTypes :[{label:'Stored Procedure',value:'storedprocedure'}],
  reportbuilderFieldResult:[{label:'Sum',value:'sum'},{label:'Avg',value:'avg'},{label:'Custom',value:'custom'}],
  reportbuilderFieldDataTypes:[{label:'String',value:'String'},{label:'Decimal',value:'Decimal'},{label:'Datetime',value:'Datetime'}],
  modules:[{id:'1',name:'Users'},{id:'2',name:'Groups'},{id:'3',name:'Permission'},{id:'4',name:'Data Audit'},{id:'5',name:'Login-Logout Audit'}
    ,{id:'6',name:'Scheduler Audit'},{id:'7',name:'Reports'},{id:'8',name:'Dashboards'},{id:'9',name:'Report Builder'},{id:'10',name:'Data Source'},{id:'11',name:'Scheduler'}
    ,{id:'12',name:'Collections'},{id:'13',name:'Configurations'}
  ],
  reportTypes :[{name:'Realtime'},{name:'Historical'}],
  reportbuilderRootPath:[{name:'Historical',id:0},{name:'RealTime',id:0}],
  reportRootPath:[{name:'Historical',id:0},{name:'RealTime',id:0}],
  reportPageSizeCollection : [5,10,20,30,40,50],
  reportPageSize :10,
  cognitoLogin : false
};
